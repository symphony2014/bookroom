﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DAL;
using DAL.Models;
using DAL.POCO.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OpenIddict.Validation;
using Microsoft.AspNetCore.Authorization;
using getYoutubeUrl;

namespace bookroomcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class YoutubeController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private UserManager<ApplicationUser> _userManager;
        // readonly ILogger _logger;
        public YoutubeController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }
        [HttpGet("[action]")]
        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        public void Fetch(string userName)
        {
            YoutubeAPI youtubeAPI = new YoutubeAPI(userName);
            string nextToken = "";
            do
            {

                var (nextTokenx, ids) = youtubeAPI.GetVideos(nextToken);
                nextToken = nextTokenx;
                foreach (var item in ids)
                {
                    _unitOfWork.Videos.Add(new Video {Title=item.Title, Description=item.Description,Key=item.ResourceId.VideoId,CreatedDate=item.PublishedAt });
                }
            } while (nextToken!=null);
                
        }
    }
}