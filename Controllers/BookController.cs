using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using DAL.POCO.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OpenIddict.Validation;
using Microsoft.AspNetCore.Authorization;
using bookroomcore.DAL.Models;

namespace bookroomcore.Controllers
{
    [Route("api/[controller]")]
       public class BookController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private UserManager<ApplicationUser> _userManager;
       // readonly ILogger _logger;
        public BookController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager) {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

            
        [HttpGet("[action]")]
        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        public IEnumerable<BookViewModel> GetBooks()
        {

            
           return  _unitOfWork.Books.GetBooks(_unitOfWork.CurrentUserId);
            //var books = _unitOfWork.Books.GetBooks(book, pageIndex, pageSize);
            //return Ok(Mapper.Map<IEnumerable<Updated>>(books));
        }
                  public IActionResult  Delete(int Id)
        {
            _unitOfWork.Books.RemoveById(Id);
            _unitOfWork.SaveChanges();
            return Ok("success");
        }
    }
}
