﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DAL.Models;
using DAL;
using Microsoft.AspNetCore.Identity;
using OpenIddict.Validation;
using Microsoft.AspNetCore.Authorization;
namespace bookroomcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
    public class ProgressController : ControllerBase
    {

        private IUnitOfWork _unitOfWork;
        private UserManager<ApplicationUser> _userManager;
        // readonly ILogger _logger;
        public ProgressController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }
        // GET: api/Progress
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Progress/5
        [HttpGet("{bookId}", Name = "Get")]
        public async Task<int> Get(int bookId)
        {

             var user = await _userManager.GetUserAsync(User).ConfigureAwait(false);

            var p =_unitOfWork.Progress.Find(pr=>pr.BookID==bookId && pr.UserID==user.Id).FirstOrDefault();
            if (p != null)
            {
                return p.value;
            }
            else
            {
                return 0;
            }
        }

        // POST: api/Progress
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Progress/5
        [HttpPut("{bookId}")]
        public Progress Put(int bookId, [FromBody] Progress value)
        {
            var p = _unitOfWork.Progress.Find(x => x.BookID == bookId && x.UserID == _unitOfWork.CurrentUserId).FirstOrDefault();
            if (p == null)
            {
                value.UserID = _unitOfWork.CurrentUserId;
                value.BookID = bookId;
                _unitOfWork.Progress.Add(value);
                _unitOfWork.SaveChanges();
                return value;
            }
            else
            {
                p.value = value.value; 
            _unitOfWork.Progress.Update(p);
            _unitOfWork.SaveChanges();
                return p;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
