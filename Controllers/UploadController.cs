
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Primitives;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using OpenIddict.Validation;
using iTextSharp.text.pdf;

namespace bookroomcore.Controllers
{
 [Produces("application/json")]
    [Route("api/[controller]")]
    public class UploadController : Controller
    {
        private IHostingEnvironment _hostingEnvironment;
        private IUnitOfWork _unitOfWork;
        private UserManager<ApplicationUser> _userManager;
        public UploadController(IHostingEnvironment hostingEnvironment, IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
        }

        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpPost, DisableRequestSizeLimit]
        public ActionResult UploadFile()
        {
            try
            {
                var file = Request.Form.Files[0];
                string folderName = "books";
                string type = "computerScience";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName,type);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string fullPath = Path.Combine(newPath, fileName);
                    string virtualPath = Path.Combine(folderName, type, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    // get pdf details.
                    var pDfReader = new PdfReader(fullPath);

                    //to db.
                   var book= _unitOfWork.Books.Add(new Book {  Title = fileName,Path=virtualPath, UserId = _unitOfWork.CurrentUserId,NumberOfPages=pDfReader.NumberOfPages});
                    _unitOfWork.Progress.Add(new Progress { BookID = book.Id, value = 0,UserID=_unitOfWork.CurrentUserId });
                    _unitOfWork.SaveChanges();

                }
                return Json("Upload Successful.");
            }
            catch (System.Exception ex)
            {
                return Json("Upload Failed: " + ex.Message);
            }
        }
    }
}