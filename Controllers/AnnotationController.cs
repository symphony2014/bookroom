﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DAL.Models;
using DAL;
using Microsoft.AspNetCore.Identity;
using OpenIddict.Validation;
using Microsoft.AspNetCore.Authorization;
using bookroomcore.DAL.Models;

namespace bookroomcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
    public class AnnotationController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private UserManager<ApplicationUser> _userManager;
        // readonly ILogger _logger;
        public AnnotationController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }
        [HttpPost]
        public IActionResult AddAnnotations(Annotation annotation)
        {
            annotation.UserID = _unitOfWork.CurrentUserId;
            _unitOfWork.Annotations.Add(annotation);
            _unitOfWork.SaveChanges();
            return Ok("success");
        }
        public IEnumerable<Annotation> GetAnnotations(int bookId)
        {
            return _unitOfWork.Annotations.Find(a => a.UserID == _unitOfWork.CurrentUserId && a.BookID == bookId);
        }
    }
}