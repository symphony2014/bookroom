﻿using Google.Apis.YouTube.v3.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;

namespace getYoutubeUrl
{
    public class YoutubeAPI
    {
        public string APIKey => "AIzaSyCFIZSjuZhHgfIQQGwzYC6GjM0rSA-QQzM";
        private string userName;
        public YoutubeAPI(string userName)
        {
            this.userName = userName;
        }
        public (string,IEnumerable<PlaylistItemSnippet>) GetVideos(string currentToken= "")

               
        {
            start: Func <string, (string, IEnumerable<PlaylistItemSnippet>)> func= (current)=>{

            var nextToken = "";
            Uri channel_api = new Uri(string.Format($"https://www.googleapis.com/youtube/v3/channels?part=snippet,contentDetails,statistics&forUsername={userName}&key={APIKey}"));
            var channels = SendRequest<ChannelListResponse>(channel_api);
            List<PlaylistItemSnippet> videos = new List<PlaylistItemSnippet>();
            foreach (var item in channels.Items)
            {
                Uri upload_api = new Uri(string.Format($@"https://www.googleapis.com/youtube/v3/channels?id={item.Id}&key={APIKey}&part=contentDetails"));
                var channel_list = SendRequest<ChannelListResponse>(upload_api);
                foreach (var channel_item in channel_list.Items)
                {
                    var uploadId = channel_item.ContentDetails.RelatedPlaylists.Uploads;
                    Uri video_api = new Uri(string.Format($@"https://www.googleapis.com/youtube/v3/playlistItems?playlistId={uploadId}&key={APIKey}&part=snippet&maxResults=50&pageToken={current}"));
                    var temp = SendRequest<PlaylistItemListResponse>(video_api);
                    nextToken = temp.NextPageToken;
                    videos.AddRange(temp.Items.Select(i =>i.Snippet));
                }
            }
            return (nextToken,videos);
            };

            try
            {
              return  func(currentToken);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                goto start;
               
            }
        }
        private T SendRequest<T>(Uri api)
        {
            Console.WriteLine("start sending...");
            WebRequest request = WebRequest.Create(api);

            WebProxy proxy = new WebProxy("localhost", 8001);
            request.Proxy = proxy;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Console.WriteLine(result);

                    return JsonConvert.DeserializeObject<T>(result);
                    //Console.WriteLine(test);
                }
            }
            Console.WriteLine("end sending...");
        }
        //public static string[] GetChannelListIds(string channelId)
        // {

        //}
    }
}
