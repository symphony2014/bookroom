﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace bookroomcore.Migrations
{
    public partial class addcreatedaate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "Videos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "Videos");
        }
    }
}
