﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bookroomcore.Migrations
{
    public partial class id : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Progress_Books_CurrentBookId",
                table: "Progress");

            migrationBuilder.DropIndex(
                name: "IX_Progress_CurrentBookId",
                table: "Progress");

            migrationBuilder.DropColumn(
                name: "CurrentBookId",
                table: "Progress");

            migrationBuilder.AlterColumn<int>(
                name: "BookID",
                table: "Progress",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Progress_BookID",
                table: "Progress",
                column: "BookID");

            migrationBuilder.AddForeignKey(
                name: "FK_Progress_Books_BookID",
                table: "Progress",
                column: "BookID",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Progress_Books_BookID",
                table: "Progress");

            migrationBuilder.DropIndex(
                name: "IX_Progress_BookID",
                table: "Progress");

            migrationBuilder.AlterColumn<string>(
                name: "BookID",
                table: "Progress",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CurrentBookId",
                table: "Progress",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Progress_CurrentBookId",
                table: "Progress",
                column: "CurrentBookId");

            migrationBuilder.AddForeignKey(
                name: "FK_Progress_Books_CurrentBookId",
                table: "Progress",
                column: "CurrentBookId",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
