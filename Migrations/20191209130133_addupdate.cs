﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace bookroomcore.Migrations
{
    public partial class addupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Annotations_Books_BookID",
            //    table: "Annotations");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Annotations_AspNetUsers_UserID",
            //    table: "Annotations");

            //migrationBuilder.DropIndex(
            //    name: "IX_Annotations_BookID",
            //    table: "Annotations");

            //migrationBuilder.DropIndex(
            //    name: "IX_Annotations_UserID",
            //    table: "Annotations");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Progress",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "Progress",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "Progress",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "Progress",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            //migrationBuilder.AlterColumn<string>(
            //    name: "UserID",
            //    table: "Annotations",
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Progress");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "Progress");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Progress");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "Progress");

            //migrationBuilder.AlterColumn<string>(
            //    name: "UserID",
            //    table: "Annotations",
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldNullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Annotations_BookID",
            //    table: "Annotations",
            //    column: "BookID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Annotations_UserID",
            //    table: "Annotations",
            //    column: "UserID");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Annotations_Books_BookID",
            //    table: "Annotations",
            //    column: "BookID",
            //    principalTable: "Books",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Annotations_AspNetUsers_UserID",
            //    table: "Annotations",
            //    column: "UserID",
            //    principalTable: "AspNetUsers",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
