﻿// ====================================================
// More Templates: https://www.ebenmonney.com/templates
// Email: support@ebenmonney.com
// ====================================================

using bookroomcore.DAL.Models;
using DAL.Models;
using DAL.Repositories;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork
    {
     
       
        string CurrentUserId { get; }
        IBookRepository Books { get; }
        Repository<Annotation> Annotations { get; }
        Repository<Video> Videos { get; }
        Repository<Progress> Progress { get; }
        int SaveChanges();
    }
}
