﻿using DAL.Models;
using DAL.POCO.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public interface IBookRepository : IRepository<Book>
    {
        IEnumerable<BookViewModel> GetBooks(string userId);

    }
}
