﻿// ====================================================
// More Templates: https://www.ebenmonney.com/templates
// Email: support@ebenmonney.com
// ====================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DAL.Models;
using DAL.Repositories.Interfaces;
using DAL.POCO.Models;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public class BookRepository : Repository<Book>,IBookRepository
    {
        public BookRepository(ApplicationDbContext context) : base(context)
        { }

   
    



        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;

        public IEnumerable<BookViewModel> GetBooks(string userId)
        {
            var data = from b in (_appContext.Books)
                       join p in _appContext.Progress on b.Id equals p.BookID into bp
                       from subset in bp.DefaultIfEmpty(new Progress { value=0,UserID=""})
                       where subset.UserID==userId
                       orderby subset.UpdatedDate descending
                       select new BookViewModel() { Title=b.Title,Progress=subset==null?0:subset.value,NumberOfPages=b.NumberOfPages, BookId=b.Id};
                       return data;
        }
    }
}
