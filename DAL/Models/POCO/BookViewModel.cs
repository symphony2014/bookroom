﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.POCO.Models
{
    public class BookViewModel
    {

        public int BookId { get; set; }
        public int Progress { get; set; }
        public int NumberOfPages { get; set; }
        public string Title { get; set; }
    }
}
