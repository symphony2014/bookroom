﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public interface IDatedEntity
    {
        DateTimeOffset Created { get; set; }
        DateTimeOffset Updated { get; set; }
    }
}
