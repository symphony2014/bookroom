﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
   public class Book
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }


        [ForeignKey("ApplicationUser.Id")]
        public string UserId { get; set; }
        public virtual ApplicationUser UploadUser { get; set; }
        public int NumberOfPages { get; internal set; }
    }
}
