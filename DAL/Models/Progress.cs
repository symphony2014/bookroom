﻿using DAL.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
   public class Progress: IAuditableEntity
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey(nameof(ApplicationUser.Id))]
        public string UserID { get; set; }
        [ForeignKey(nameof(Book.Id))]
        public int BookID { get; set; }
        public int value { get; set; }
        public   virtual ApplicationUser User { get; set; }
        public virtual Book CurrentBook { get; set; }
        public string CreatedBy { get; set ; }
        public string UpdatedBy { get ; set ; }
        public DateTime CreatedDate { get ; set ; }
        public DateTime UpdatedDate { get ; set ; }
    }
}
