﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class Video
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Language { get; set; }
        public bool Uploaded { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
