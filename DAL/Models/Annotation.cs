﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace bookroomcore.DAL.Models
{
    public class Annotation
    {
        [Key]
        public int Id { get; set; }
        public string UserID { get; set; }
        public int BookID { get; set; }

        public string JSON { get; set; }

    }
}
