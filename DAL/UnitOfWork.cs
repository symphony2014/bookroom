﻿// ====================================================
// More Templates: https://www.ebenmonney.com/templates
// Email: support@ebenmonney.com
// ====================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bookroomcore.DAL.Models;
using DAL.Models;
using DAL.Repositories;
using DAL.Repositories.Interfaces;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

 
        IBookRepository _book;
        Repository<Progress> _progress;
        Repository<Annotation> _annotation;
        Repository<Video> _videos;


        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }




        public string CurrentUserId
        {
            get
            {
                return _context.CurrentUserId;
            }
        }
        public IBookRepository Books
        {
            get
            {
                if (_book == null)
                    _book = new BookRepository(_context);

                return _book;
            }
        }

        public Repository<Progress> Progress
        {
            get
            {
                if (_progress == null)
                    _progress = new Repository<Progress>(_context);

                return _progress;
            }
        }
        public Repository<Video> Videos
        {
            get
            {
                if (_videos == null)
                    _videos = new Repository<Video>(_context);

                return _videos;
            }
        }
        public Repository<Annotation> Annotations
        {
            get
            {
                if (_annotation== null)
                    _annotation = new Repository<Annotation>(_context);

                return _annotation;
            }
        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
