################################################################
#
# MysqlBackup.ps1
# Author: Ardian
# Updated: Nov 1, 2018
#
# Description:
# This script will query all MySQL databases and then create .sql backup files
# of all located databases.
#
################################################################
 
#Set up your parameter
 
$MYSQL_DIR = "C:\Program Files\MySQL\MySQL Server 8.0\bin"
$BACKUP_FOLDER = ".\"
$dbuser = 'root'
$dbpass = 'Yu9Y6RkmwPp8Xe0'
$BACKUPDATE = Get-Date -Format FileDate
 

 
# Query and backup MySQL Databases
 
Set-Location "$MYSQL_DIR\bin"
.\mysqldump.exe --defaults-file=.\config.cnf –u root library > D:\projects\bookroomcore\ClientApp\library.sql
Set-Location "D:\projects\bookroomcore\ClientApp"
# END OF SCRIPT