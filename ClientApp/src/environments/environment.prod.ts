export const environment = {
  SERVER_URL: `./`,
  production: true,
  useHash: true,
  hmr: false,
  API_URL: 'http://3.82.246.1:8081',
  token_API: 'http://3.82.246.1:8081/connect/token?_allow_anonymous=true',
  pdf_URL: '/api/PdfViewer',
};
