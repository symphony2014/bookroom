import { Injectable } from '@angular/core';
import { RefreshGrantModel } from './models/refresh-grant-model';
import { ProfileModel } from './models/profile-model';
import { AuthStateModel } from './models/auth-state-model';
import { AuthTokenModel } from './models/auth-tokens-model';
import { RegisterModel } from './models/register-model';
import { LoginModel } from './models/login-model';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Subscription } from 'rxjs/internal/Subscription';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { flatMap, first, filter, tap, map } from 'rxjs/operators';
import { interval } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '@env/environment';
const jwtDecode = require('jwt-decode');

@Injectable()
export class AuthService {
  private initalState: AuthStateModel = { profile: null, tokens: null, authReady: false };
  private authReady$ = new BehaviorSubject<boolean>(false);
  private state: BehaviorSubject<AuthStateModel>;
  private refreshSubscription$: Subscription;

  state$: Observable<AuthStateModel>;
  tokens$: Observable<AuthTokenModel>;
  profile$: Observable<ProfileModel>;
  loggedIn$: Observable<boolean>;

  constructor(private http: HttpClient) {
    this.state = new BehaviorSubject<AuthStateModel>(this.initalState);
    this.state$ = this.state.asObservable();

    this.tokens$ = this.state.pipe(
      filter(state => state.authReady),
      map(state => state.tokens),
    );

    this.profile$ = this.state.pipe(
      filter(state => state.authReady),
      map(state => state.profile),
    );

    this.loggedIn$ = this.tokens$.pipe(map(tokens => !!tokens));
  }
  init(): Observable<AuthTokenModel> {
    return this.startupTokenRefresh().pipe(tap(() => this.scheduleRefresh()));
  }

  register(data: RegisterModel): Observable<any> {
    return this.http.post(`${environment.API_URL}/account/register`, data);
  }

  login(user: LoginModel): Observable<any> {
    return this.getTokens(user, 'password').pipe(tap(res => this.scheduleRefresh()));
  }

  logout(): void {
    this.updateState({ profile: null, tokens: null });
    if (this.refreshSubscription$) {
      this.refreshSubscription$.unsubscribe();
    }
    this.removeToken();
  }

  refreshTokens(): Observable<AuthTokenModel> {
    return this.state.pipe(
      first(),
      map(state => state.tokens),
      flatMap(tokens => this.getTokens({ refresh_token: tokens.refresh_token }, 'refresh_token')),
    );
  }

  private storeToken(tokens: AuthTokenModel): void {
    const previousTokens = this.retrieveTokens();
    if (previousTokens != null && tokens.refresh_token == null) {
      tokens.refresh_token = previousTokens.refresh_token;
    }

    localStorage.setItem('auth-tokens', JSON.stringify(tokens));
  }

  private retrieveTokens(): AuthTokenModel {
    const tokensString = localStorage.getItem('auth-tokens');
    const tokensModel: AuthTokenModel = tokensString == null ? null : JSON.parse(tokensString);
    return tokensModel;
  }

  private removeToken(): void {
    localStorage.removeItem('auth-tokens');
  }

  private updateState(newState: AuthStateModel): void {
    const previousState = this.state.getValue();
    this.state.next({ ...previousState, ...newState });
  }

  private getTokens(data: RefreshGrantModel | LoginModel, grantType: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    Object.assign(data, { grant_type: grantType, scope: 'openid offline_access' });

    const params = new HttpParams()
      .append('username', (data as LoginModel).username)
      .append('password', (data as LoginModel).password)
      .append('grant_type', 'password')
      .append('client_id', 'web')
      .append('client_secret', '388D45FA-B36B-4988-BA59-B187D329C207');
    // .append('scope', 'openid email phone profile offline_access roles');

    const requestBody = params.toString();
    return this.http.post(
      `${environment.API_URL}/connect/token?_allow_anonymous=true&${requestBody}`,
      params.toString(),
      { headers },
    );
  }

  private startupTokenRefresh(): Observable<AuthTokenModel> {
    return of(this.retrieveTokens()).pipe(
      flatMap((tokens: AuthTokenModel) => {
        if (!tokens) {
          this.updateState({ authReady: true });
          return Observable.throw('No token in Storage');
        }
        const profile: ProfileModel = jwtDecode(tokens.id_token);
        this.updateState({ tokens, profile });

        if (+tokens.expiration_date > new Date().getTime()) {
          this.updateState({ authReady: true });
        }

        return this.refreshTokens();
      }),
    );
  }

  private scheduleRefresh(): void {
    this.refreshSubscription$ = this.tokens$
      .pipe(
        first(),
        flatMap(tokens => interval(((tokens as any).expires_in / 2) * 1000)),
        flatMap(() => this.refreshTokens()),
      )
      .subscribe();
    // refresh every half the total expiration time
  }
}
