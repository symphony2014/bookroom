/**
 * My Title
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OneOfBookUploadUser } from './oneOfBookUploadUser';

export interface Book { 
    id?: number;
    title?: string;
    path?: string;
    userId?: string;
    uploadUser?: OneOfBookUploadUser;
    numberOfPages?: number;
}