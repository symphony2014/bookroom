import { NgModule } from '@angular/core';

import { SharedModule } from '@shared';
import { ManagementRoutingModule } from './management-routing.module';

import { ProProfileBaseComponent } from './profile/basic/basic.component';
import { ProProfileAdvancedComponent } from './profile/advanced/advanced.component';
import { ProAccountCenterComponent } from './account/center/center.component';
import { ProAccountCenterArticlesComponent } from './account/center/articles/articles.component';
import { ProAccountCenterApplicationsComponent } from './account/center/applications/applications.component';
import { ProAccountCenterProjectsComponent } from './account/center/projects/projects.component';
import { ProAccountSettingsComponent } from './account/settings/settings.component';
import { ProAccountSettingsBaseComponent } from './account/settings/base/base.component';
import { ProAccountSettingsSecurityComponent } from './account/settings/security/security.component';
import { ProAccountSettingsBindingComponent } from './account/settings/binding/binding.component';
import { ProAccountSettingsNotificationComponent } from './account/settings/notification/notification.component';
import { BookListComponent } from './list/book-list/book-list.component';
import { ReadBookComponent } from './read-book/read-book.component';
import { ButtonModule, CheckBoxModule, Button } from '@syncfusion/ej2-angular-buttons';

import { ToolbarModule } from '@syncfusion/ej2-angular-navigations';

import { DialogModule } from '@syncfusion/ej2-angular-popups';

import { PdfViewerModule } from '@syncfusion/ej2-angular-pdfviewer';
const COMPONENTS = [
  ProProfileBaseComponent,
  ProProfileAdvancedComponent,
  ProAccountCenterComponent,
  ProAccountCenterArticlesComponent,
  ProAccountCenterProjectsComponent,
  ProAccountCenterApplicationsComponent,
  ProAccountSettingsComponent,
  ProAccountSettingsBaseComponent,
  ProAccountSettingsSecurityComponent,
  ProAccountSettingsBindingComponent,
  ProAccountSettingsNotificationComponent,
];

const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [CheckBoxModule, ToolbarModule, DialogModule, PdfViewerModule, SharedModule, ManagementRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT, BookListComponent, ReadBookComponent],
  entryComponents: COMPONENTS_NOROUNT,
})
export class ManagementModule {}
