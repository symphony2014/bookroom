import { Component, OnInit, Optional, Inject, HostListener, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BASE_PATH, ProgressService, AnnotationService as AS } from 'src/api';
import { Subject, fromEvent } from 'rxjs';
import { throttleTime, debounceTime, combineLatest, merge, tap, switchMap, zip } from 'rxjs/operators';
import {
  LinkAnnotationService,
  BookmarkViewService,
  MagnificationService,
  ThumbnailViewService,
  ToolbarService,
  NavigationService,
  AnnotationService,
  TextSearchService,
  TextSelectionService,
  PrintService,
} from '@syncfusion/ej2-angular-pdfviewer';
import { environment } from '@env/environment';
import { ITextMarkupAnnotation } from '@shared/pdfviewer-component/pdfviewer';

@Component({
  selector: 'app-read-book',
  templateUrl: './read-book.component.html',
  styleUrls: ['./read-book.component.less'],
  providers: [
    LinkAnnotationService,
    BookmarkViewService,
    MagnificationService,
    ThumbnailViewService,
    ToolbarService,
    NavigationService,
    AnnotationService,
    TextSearchService,
    TextSelectionService,
    PrintService,
  ],
})
export class ReadBookComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    @Optional() @Inject(BASE_PATH) private basePath: string,
    private progressService: ProgressService,
    private annotationService: AS,
  ) {}
  id: number;
  currentPageIndex: number;
  path: string;
  public scroll = false;
  private timeout: number;
  public service = environment.pdf_URL;
  @ViewChild('pdf', { static: true }) pdf;
  $pageChange: Subject<any> = new Subject<any>();
  $documentLoad: Subject<any> = new Subject<any>();
  ngOnInit() {
    this.$pageChange.pipe(debounceTime(350)).subscribe($event => {
      this.progressService.progressPut({ value: $event.currentPageNumber }, this.id).subscribe(res => {
        console.log(res);
      });
    });
    this.route.params
      .pipe(
        tap(param => {
          this.id = param.id;
          this.path = `${this.basePath}/api/Book/GetBook?id=${param.id}`;
        }),
        switchMap(param => this.progressService.progressGet(param.id)),
        zip(this.$documentLoad),
        debounceTime(1000),
        tap(res => {
          this.currentPageIndex = res[0];
          this.pdf.navigationModule.goToPage(res[0]);
        }),
        switchMap(res => {
          return this.annotationService.annotationGetAnnotations(this.id);
        }),
        debounceTime(1000),
      )
      .subscribe(res => {
        console.log(this.pdf.annotationModule.textMarkupAnnotationModule.renderTextMarkupAnnotationsInPage);
        this.pdf.magnification.zoomTo(125);
        this.pdf.annotationModule.textMarkupAnnotationModule.saveAnnotations(
          res.map(r => {
            const obj = JSON.parse(r.json);
            return {
              textMarkupAnnotationType: obj.annotationType,
              bounds: obj.annotationBound,
              color: obj.annotationSettings.color,
              opacity: obj.annotationSettings.opacity,
            };
          }) as ITextMarkupAnnotation[],
          this.currentPageIndex - 1,
        );
        // this.pdf
      });
  }
  pageChange($event) {
    this.$pageChange.next($event);
  }
  documentLoad($event) {
    this.$documentLoad.next($event);
  }
  annotationAdd($event) {
    console.log($event);
    this.annotationService.annotationAddAnnotations({ bookID: this.id, json: JSON.stringify($event) }).subscribe();
  }
}
