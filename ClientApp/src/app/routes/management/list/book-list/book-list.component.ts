import { Component, OnInit, ChangeDetectorRef, Optional, Inject } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd';
import { BASE_PATH, UploadService, BookService, Book, BookViewModel } from 'src/api';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.less'],
})
export class BookListComponent implements OnInit {
  uploadUrl = `${this.basePath}/api/Upload`;
  list: BookViewModel[] = [null];

  loading = true;

  constructor(
    private http: _HttpClient,
    public msg: NzMessageService,
    @Optional() @Inject(BASE_PATH) private basePath: string,
    private cdr: ChangeDetectorRef,
    private api: BookService,
  ) {}

  ngOnInit() {
    this.update();
  }
  change($event) {
    if ($event.file.status === 'done') {
      this.update();
    }
  }
  delete(id: number) {
    this.api.bookDelete(id).subscribe(res => this.update());
  }
  update() {
    this.loading = true;
    this.api.bookGetBooks().subscribe(res => {
      this.list = res;
      this.cdr.detectChanges();
      this.loading = false;
    });
  }
}
