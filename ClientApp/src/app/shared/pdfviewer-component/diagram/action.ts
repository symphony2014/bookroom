import { IElement } from '@syncfusion/ej2-drawings';
import { PointModel } from '@syncfusion/ej2-drawings';
import { Rect } from '@syncfusion/ej2-drawings';
import { DrawingElement } from '@syncfusion/ej2-drawings';
import { Container } from '@syncfusion/ej2-drawings';
import { PdfViewerBase } from '../pdfviewer';
import { PdfAnnotationBaseModel, PdfBoundsModel } from './pdf-annotation-model';
import { ZOrderPageTable } from './pdf-annotation';
import { isPointOverConnector } from './connector-util';
import { PdfViewer } from '../pdfviewer/pdfviewer';

/** @private */
export function findActiveElement(
  event: MouseEvent | TouchEvent,
  pdfBase: PdfViewerBase,
  pdfViewer: PdfViewer,
): IElement {
  if (pdfViewer && pdfBase.activeElements.activePageID > -1) {
    const objects: IElement[] = findObjectsUnderMouse(pdfBase, pdfViewer, event as MouseEvent);
    const object: IElement = findObjectUnderMouse(objects, event, pdfBase, pdfViewer);
    return object;
  }
  return undefined;
}

/** @private */
export function findObjectsUnderMouse(pdfBase: PdfViewerBase, pdfViewer: PdfViewer, event: MouseEvent): IElement[] {
  const actualTarget: IElement[] = [];
  let bounds: Rect;
  // tslint:disable-next-line
  let pt: PointModel = pdfBase.currentPosition || { x: event.offsetX, y: event.offsetY };
  pt = { x: pt.x / pdfBase.getZoomFactor(), y: pt.y / pdfBase.getZoomFactor() };
  const pageTable: ZOrderPageTable = pdfViewer.getPageTable(pdfBase.activeElements.activePageID);
  const objArray: Object[] = findObjects(pt, pageTable.objects);
  return objArray as IElement[];
}

/** @private */
export function findObjectUnderMouse(
  // tslint:disable-next-line
  objects: (PdfAnnotationBaseModel)[],
  event: any,
  pdfBase: PdfViewerBase,
  pdfViewer: PdfViewer,
): IElement {
  let actualTarget: PdfAnnotationBaseModel = null;
  let touchArg: TouchEvent;
  let offsetX: number;
  let offsetY: number;
  if (event && event.type && event.type.indexOf('touch') !== -1) {
    touchArg = event as TouchEvent & PointerEvent;
    if (pdfViewer.annotation) {
      const pageDiv: HTMLElement = pdfBase.getElement('_pageDiv_' + pdfViewer.annotation.getEventPageNumber(event));
      if (pageDiv) {
        const pageCurrentRect: ClientRect = pageDiv.getBoundingClientRect();

        offsetX = touchArg.changedTouches[0].clientX - pageCurrentRect.left;
        offsetY = touchArg.changedTouches[0].clientY - pageCurrentRect.top;
      }
    }
  } else {
    offsetX = !isNaN(event.offsetX) ? event.offsetX : event.position ? event.position.x : 0;
    offsetY = !isNaN(event.offsetY) ? event.offsetY : event.position ? event.position.y : 0;
  }
  const offsetForSelector = 5;
  for (let i = 0; i < objects.length; i++) {
    // tslint:disable-next-line:max-line-length
    if (
      !(
        objects[i].shapeAnnotationType === 'Distance' ||
        objects[i].shapeAnnotationType === 'Line' ||
        objects[i].shapeAnnotationType === 'LineWidthArrowHead'
      )
    ) {
      const bounds: PdfBoundsModel = objects[i].wrapper.bounds;
      let rotationValue = 0;
      if (objects[i].shapeAnnotationType === 'Stamp') {
        rotationValue = 25;
      }
      // tslint:disable-next-line:max-line-length
      if (
        (bounds.x - offsetForSelector) * pdfBase.getZoomFactor() < offsetX &&
        (bounds.x + bounds.width + offsetForSelector) * pdfBase.getZoomFactor() > offsetX &&
        (bounds.y - offsetForSelector - rotationValue) * pdfBase.getZoomFactor() < offsetY &&
        (bounds.y + bounds.height + offsetForSelector) * pdfBase.getZoomFactor() > offsetY
      ) {
        actualTarget = objects[i];
        break;
      }
    } else {
      const pt: PointModel = { x: offsetX / pdfBase.getZoomFactor(), y: offsetY / pdfBase.getZoomFactor() };
      const obj: DrawingElement = findElementUnderMouse(objects[i] as IElement, pt, offsetForSelector);
      const isOver: boolean = isPointOverConnector(objects[i], pt);
      if (obj && isOver) {
        actualTarget = objects[i];
        break;
      }
    }
  }
  return actualTarget as IElement;
}
/** @private */
export function findElementUnderMouse(obj: IElement, position: PointModel, padding?: number): DrawingElement {
  return findTargetShapeElement(obj.wrapper, position, padding);
}
/** @private */
export function insertObject(obj: PdfAnnotationBaseModel, key: string, collection: Object[]): void {
  if (collection.length === 0) {
    collection.push(obj);
  } else if (collection.length === 1) {
    // tslint:disable-next-line
    if ((collection[0] as any)[key] > (obj as any)[key]) {
      collection.splice(0, 0, obj);
    } else {
      collection.push(obj);
    }
  } else if (collection.length > 1) {
    let low = 0;
    let high: number = collection.length - 1;
    let mid: number = Math.floor((low + high) / 2);
    while (mid !== low) {
      // tslint:disable-next-line
      if ((collection[mid] as any)[key] < (obj as any)[key]) {
        low = mid;
        mid = Math.floor((low + high) / 2);
        // tslint:disable-next-line
      } else if ((collection[mid] as any)[key] > (obj as any)[key]) {
        high = mid;
        mid = Math.floor((low + high) / 2);
      }
    }
    // tslint:disable-next-line
    if ((collection[high] as any)[key] < (obj as any)[key]) {
      collection.push(obj);
      // tslint:disable-next-line
    } else if ((collection[low] as any)[key] > (obj as any)[key]) {
      collection.splice(low, 0, obj);
      // tslint:disable-next-line
    } else if (
      (collection[low] as any)[key] < (obj as any)[key] &&
      (collection[high] as any)[key] > (obj as any)[key]
    ) {
      collection.splice(high, 0, obj);
    }
  }
}

/** @private */
export function findTargetShapeElement(container: Container, position: PointModel, padding?: number): DrawingElement {
  if (container && container.children) {
    for (let i: number = container.children.length - 1; i >= 0; i--) {
      const shapeElement: DrawingElement = container.children[i];
      if (shapeElement && shapeElement.bounds.containsPoint(position, 10)) {
        if (shapeElement instanceof Container) {
          const targetElement: DrawingElement = this.findTargetElement(shapeElement, position);
          if (targetElement) {
            return targetElement;
          }
        }
        if (shapeElement.bounds.containsPoint(position, 10)) {
          return shapeElement;
        }
      }
    }
  }

  if (container.bounds.containsPoint(position, padding) && container.style.fill !== 'none') {
    return container;
  }
  return null;
}

/** @private */
export function findObjects(region: PointModel, objCollection: (PdfAnnotationBaseModel)[]): (PdfAnnotationBaseModel)[] {
  const objects: (PdfAnnotationBaseModel)[] = [];
  for (const obj of objCollection) {
    if (
      findElementUnderMouse(obj as IElement, region, 10) ||
      (obj.shapeAnnotationType === 'Stamp' && findElementUnderMouse(obj as IElement, region, 40))
    ) {
      insertObject(obj, 'Zindex', objects);
    }
  }
  return objects;
}

/** @private */
export function findActivePage(event: MouseEvent, pdfBase: PdfViewerBase): number {
  let activePageID: number;
  if (event.target && (event.target as PdfAnnotationBaseModel).wrapper) {
    return (event.target as PdfAnnotationBaseModel).pageIndex;
  }
  if (event.target) {
    const elementIdColl: string[] = (event.target as HTMLElement).id.split('_');
    if (elementIdColl.length > 0) {
      // tslint:disable-next-line:radix
      activePageID = parseInt(elementIdColl[elementIdColl.length - 1]);
    }
  }
  return activePageID;
}
/**
 * @hidden
 */
export class ActiveElements {
  private activePage: number = undefined;
  /** @private */
  public get activePageID(): number {
    return this.activePage;
  }

  /** @private */
  public set activePageID(offset: number) {
    this.activePage = offset;
    // tslint:disable-next-line
    if (offset !== this.activePage) {
    }
  }

  constructor() {
    this.activePageID = undefined;
  }
}
