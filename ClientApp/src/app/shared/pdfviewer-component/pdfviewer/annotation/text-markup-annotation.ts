import {
     PdfViewerBase, IRectangle, ISelection, AnnotationType, IPageAnnotations, ICommentsCollection,
    IReviewCollection
} from '../index';
import { createElement, Browser, isNullOrUndefined } from '@syncfusion/ej2-base';
import { ColorPicker, ChangeEventArgs } from '@syncfusion/ej2-inputs';
import { PdfViewer } from '../pdfviewer';

/**
 * @hidden
 */
export interface ITextMarkupAnnotation {
    textMarkupAnnotationType: string;
    author: string;
    subject: string;
    modifiedDate: string;
    note: string;
    // tslint:disable-next-line
    bounds: any;
    // tslint:disable-next-line
    color: any;
    opacity: number;
    // tslint:disable-next-line
    rect: any;
    comments: ICommentsCollection[];
    review: IReviewCollection;
    annotName: string;
    shapeAnnotationType: string;
    position?: string;
}

/**
 * @hidden
 */
export interface IPageAnnotationBounds {
    pageIndex: number;
    bounds: IRectangle[];
    // tslint:disable-next-line
    rect: any;
    startIndex?: number;
    endIndex?: number;
    textContent?: string;
}

/**
 * The `TextMarkupAnnotation` module is used to handle text markup annotation actions of PDF viewer.
 * @hidden
 */
export class TextMarkupAnnotation {
    private pdfViewer: PdfViewer;
    private pdfViewerBase: PdfViewerBase;
    /**
     * @private
     */
    public isTextMarkupAnnotationMode: boolean;
    /**
     * @private
     */
    public currentTextMarkupAddMode = '';
    /**
     * @private
     */
    public highlightColor: string;
    /**
     * @private
     */
    public underlineColor: string;
    /**
     * @private
     */
    public strikethroughColor: string;
    /**
     * @private
     */
    public highlightOpacity: number;
    /**
     * @private
     */
    public underlineOpacity: number;
    /**
     * @private
     */
    public strikethroughOpacity: number;
    /**
     * @private
     */
    public selectTextMarkupCurrentPage: number = null;
    /**
     * @private
     */
    public currentTextMarkupAnnotation: ITextMarkupAnnotation = null;
    private currentAnnotationIndex: number = null;
    private isAnnotationSelect = false;
    // tslint:disable-next-line
    private dropDivAnnotationLeft: any;
    // tslint:disable-next-line
    private dropDivAnnotationRight: any;
    // tslint:disable-next-line
    private dropElementLeft: any;
    // tslint:disable-next-line
    private dropElementRight: any;
    /**
     * @private
     */
    public isDropletClicked = false;
    /**
     * @private
     */
    public isRightDropletClicked = false;
    /**
     * @private
     */
    public isLeftDropletClicked = false;
    private isSelectionMaintained = false;
    private isExtended = false;

    /**
     * @private
     */
    constructor(pdfViewer: PdfViewer, viewerBase: PdfViewerBase) {
        this.pdfViewer = pdfViewer;
        this.pdfViewerBase = viewerBase;
        this.highlightColor = pdfViewer.highlightSettings.color;
        this.underlineColor = pdfViewer.underlineSettings.color;
        this.strikethroughColor = pdfViewer.strikethroughSettings.color;
        this.highlightOpacity = pdfViewer.highlightSettings.opacity;
        this.underlineOpacity = pdfViewer.underlineSettings.opacity;
        this.strikethroughOpacity = pdfViewer.strikethroughSettings.opacity;
    }
    /**
     * @private
     */
    public createAnnotationSelectElement(): void {
        // tslint:disable-next-line:max-line-length
        this.dropDivAnnotationLeft = createElement('div', { id: this.pdfViewer.element.id + '_droplet_left', className: 'e-pv-drop' });
        this.dropDivAnnotationLeft.style.borderRight = '2px solid';
        // tslint:disable-next-line:max-line-length
        this.dropDivAnnotationRight = createElement('div', { id: this.pdfViewer.element.id + '_droplet_right', className: 'e-pv-drop' });
        this.dropDivAnnotationRight.style.borderLeft = '2px solid';
        this.dropElementLeft = createElement('div', { className: 'e-pv-droplet', id: this.pdfViewer.element.id + '_dropletspan_left', });
        this.dropElementLeft.style.transform = 'rotate(0deg)';
        this.dropDivAnnotationLeft.appendChild(this.dropElementLeft);
        this.dropElementRight = createElement('div', { className: 'e-pv-droplet', id: this.pdfViewer.element.id + '_dropletspan_right' });
        this.dropElementRight.style.transform = 'rotate(-90deg)';
        this.dropDivAnnotationRight.appendChild(this.dropElementRight);
        this.pdfViewerBase.pageContainer.appendChild(this.dropDivAnnotationLeft);
        this.pdfViewerBase.pageContainer.appendChild(this.dropDivAnnotationRight);
        this.dropElementLeft.style.top = '20px';
        this.dropElementRight.style.top = '20px';
        this.dropElementRight.style.left = '-8px';
        this.dropElementLeft.style.left = '-8px';
        this.dropDivAnnotationLeft.style.display = 'none';
        this.dropDivAnnotationRight.style.display = 'none';
        this.dropDivAnnotationLeft.addEventListener('mousedown', this.maintainSelection);
        this.dropDivAnnotationLeft.addEventListener('mousemove', this.annotationLeftMove);
        this.dropDivAnnotationLeft.addEventListener('mouseup', this.selectionEnd);
        this.dropDivAnnotationRight.addEventListener('mousedown', this.maintainSelection);
        this.dropDivAnnotationRight.addEventListener('mousemove', this.annotationRightMove);
        this.dropDivAnnotationRight.addEventListener('mouseup', this.selectionEnd);
    }
    // tslint:disable-next-line
    private maintainSelection = (event: any): void => {
        this.isDropletClicked = true;
        this.pdfViewer.textSelectionModule.initiateSelectionByTouch();
        this.isExtended = true;
        this.pdfViewer.textSelectionModule.selectionRangeArray = [];
    }
    // tslint:disable-next-line
    private selectionEnd = (event: any): void => {
        if (this.isDropletClicked) {
            this.isDropletClicked = false;
        }
    }
    // tslint:disable-next-line
    private annotationLeftMove = (event: any): void => {
        if (this.isDropletClicked) {
            this.isLeftDropletClicked = true;
        }
    }

    // tslint:disable-next-line
    private annotationRightMove = (event: any): void => {
        if (this.isDropletClicked) {
            this.isRightDropletClicked = true;
        }
    }
    /**
     * @private
     */
    // tslint:disable-next-line
    public textSelect(target: any, x: any, y: any) {
        if (this.isLeftDropletClicked) {
            const leftElement: ClientRect = this.dropDivAnnotationRight.getBoundingClientRect();
            const clientX: number = x;
            const clientY: number = y;
            if (target.classList.contains('e-pv-text')) {
                this.pdfViewer.textSelectionModule.textSelectionOnDrag(target, clientX, clientY, false);
                this.updateLeftposition(clientX, clientY);
            }
        } else if (this.isRightDropletClicked) {
            const leftElement: ClientRect = this.dropDivAnnotationLeft.getBoundingClientRect();
            const clientX: number = x;
            const clientY: number = y;
            if (target.classList.contains('e-pv-text')) {
                if (clientY >= leftElement.top) {
                    this.pdfViewer.textSelectionModule.textSelectionOnDrag(target, clientX, clientY, true);
                } else {
                    this.pdfViewer.textSelectionModule.textSelectionOnDrag(target, clientX, clientY, false);
                }
                this.updatePosition(clientX, clientY);
            }
        }
    }
    /**
     * @private
     */
    public showHideDropletDiv(hide: boolean): void {
        if (this.pdfViewer.enableTextMarkupResizer && this.dropDivAnnotationLeft && this.dropDivAnnotationRight) {
            if (hide) {
                this.dropDivAnnotationLeft.style.display = 'none';
                this.dropDivAnnotationRight.style.display = 'none';
            } else {
                this.dropDivAnnotationLeft.style.display = '';
                this.dropDivAnnotationRight.style.display = '';
            }
        }
    }
    private updateAnnotationBounds(): void {
        this.isSelectionMaintained = false;
        // tslint:disable-next-line
        let annotation: any = this.currentTextMarkupAnnotation;
        if (annotation && annotation.bounds) {
            this.retreieveSelection(annotation, null);
            this.pdfViewer.textSelectionModule.maintainSelection(this.selectTextMarkupCurrentPage, false);
            this.isSelectionMaintained = true;
            window.getSelection().removeAllRanges();
        }
    }

    // tslint:disable-next-line
    private retreieveSelection(annotation: any, element: any): any {
        for (let k = 0; k < annotation.bounds.length; k++) {
            // tslint:disable-next-line
            let bound: any = annotation.bounds[k];
            const x: number = (bound.left ? bound.left : bound.Left) * this.pdfViewerBase.getZoomFactor();
            const y: number = (bound.top ? bound.top : bound.Top) * this.pdfViewerBase.getZoomFactor();
            const width: number = (bound.width ? bound.width : bound.Width) * this.pdfViewerBase.getZoomFactor();
            const height: number = bound.height ? bound.height : bound.Height;
            // tslint:disable-next-line
            let textDivs: any = this.pdfViewerBase.getElement('_textLayer_' + this.selectTextMarkupCurrentPage).childNodes;
            for (let n = 0; n < textDivs.length; n++) {
                if (textDivs[n]) {
                    // tslint:disable-next-line
                    let top: number = parseInt(textDivs[n].style.top);
                    // tslint:disable-next-line
                    if (top === parseInt(y.toString()) || (top + 1) === parseInt(y.toString()) || (top - 1) === parseInt(y.toString())) {
                        element = textDivs[n];
                        break;
                    }
                }
            }
            if (element != null) {
                // tslint:disable-next-line
                let boundingRect: any = this.pdfViewerBase.getElement('_textLayer_' + this.selectTextMarkupCurrentPage).getBoundingClientRect();
                this.pdfViewer.textSelectionModule.textSelectionOnMouseMove(element, x + boundingRect.left, y + boundingRect.top);
                if ((annotation.bounds.length - 1) === k) {
                    // tslint:disable-next-line:max-line-length
                    this.pdfViewer.textSelectionModule.textSelectionOnMouseMove(element, x + boundingRect.left + width, y + boundingRect.top);
                }
            }
        }
    }
    /**
     * @private
     */
    public updatePosition(x: number, y: number, isSelected?: boolean): void {
        this.showHideDropletDiv(false);
        const pageTopValue: number = this.pdfViewerBase.pageSize[this.pdfViewerBase.currentPageNumber - 1].top;
        const topClientValue: number = this.getClientValueTop(y, this.pdfViewerBase.currentPageNumber - 1);
        // tslint:disable-next-line
        let rightDivElement: any = document.getElementById(this.pdfViewer.element.id + '_droplet_right');
        if (isSelected) {
            // tslint:disable-next-line:max-line-length
            rightDivElement.style.top = topClientValue * this.pdfViewerBase.getZoomFactor() + pageTopValue * this.pdfViewerBase.getZoomFactor() + 'px';
        } else {
            // tslint:disable-next-line:max-line-length
            rightDivElement.style.top = topClientValue + pageTopValue * this.pdfViewerBase.getZoomFactor() + 'px';
        }
        rightDivElement.style.left = x - this.pdfViewerBase.viewerContainer.getBoundingClientRect().left + 'px';
    }
    /**
     * @private
     */
    public updateLeftposition(x: number, y: number, isSelected?: boolean): void {
        this.showHideDropletDiv(false);
        const pageTopValue: number = this.pdfViewerBase.pageSize[this.pdfViewerBase.currentPageNumber - 1].top;
        const topClientValue: number = this.getClientValueTop(y, this.pdfViewerBase.currentPageNumber - 1);
        // tslint:disable-next-line
        let leftDivElement: any = document.getElementById(this.pdfViewer.element.id + '_droplet_left');
        leftDivElement.style.display = '';
        if (isSelected) {
            // tslint:disable-next-line:max-line-length
            leftDivElement.style.top = topClientValue * this.pdfViewerBase.getZoomFactor() + pageTopValue * this.pdfViewerBase.getZoomFactor() + 'px';
        } else {
            // tslint:disable-next-line:max-line-length
            leftDivElement.style.top = topClientValue + pageTopValue * this.pdfViewerBase.getZoomFactor() + 'px';
        }
        leftDivElement.style.left = x - this.pdfViewerBase.viewerContainer.getBoundingClientRect().left - 20 + 'px';
    }


    private getClientValueTop(clientValue: number, pageNumber: number): number {
        // tslint:disable-next-line:max-line-length
        return clientValue - this.pdfViewerBase.getElement('_pageDiv_' + pageNumber).getBoundingClientRect().top;
    }
    /**
     * @private
     */
    // tslint:disable-next-line
    public renderTextMarkupAnnotationsInPage(textMarkupAnnotations: any, pageNumber: number, isImportTextMarkup?: boolean): void {
        const canvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageNumber);
        if (isImportTextMarkup) {
            this.renderTextMarkupAnnotations(null, pageNumber, canvas, this.pdfViewerBase.getZoomFactor());
            this.renderTextMarkupAnnotations(textMarkupAnnotations, pageNumber, canvas, this.pdfViewerBase.getZoomFactor(), true);
        } else {
            this.renderTextMarkupAnnotations(textMarkupAnnotations, pageNumber, canvas, this.pdfViewerBase.getZoomFactor());
        }
    }

    // tslint:disable-next-line
    private renderTextMarkupAnnotations(textMarkupAnnotations: any, pageNumber: number, canvas: HTMLElement, factor: number, isImportAction?: boolean): void {
        if (canvas) {
            const context: CanvasRenderingContext2D = (canvas as HTMLCanvasElement).getContext('2d');
            context.setTransform(1, 0, 0, 1, 0, 0);
            context.setLineDash([]);
            // tslint:disable-next-line
            let annotations: any[];
            if (!isImportAction) {
                annotations = this.getAnnotations(pageNumber, textMarkupAnnotations);
            } else {
                annotations = textMarkupAnnotations;
            }
            if (annotations) {
                for (let i = 0; i < annotations.length; i++) {
                    // tslint:disable-next-line
                    let annotation: any = annotations[i];
                    let annotationObject: ITextMarkupAnnotation = null;
                    if (annotation.TextMarkupAnnotationType) {
                        // tslint:disable-next-line:max-line-length
                        annotationObject = {
                            textMarkupAnnotationType: annotation.TextMarkupAnnotationType, color: annotation.Color, opacity: annotation.Opacity, bounds: annotation.Bounds, author: annotation.Author, subject: annotation.Subject, modifiedDate: annotation.ModifiedDate, note: annotation.Note, rect: annotation.Rect,
                            // tslint:disable-next-line:max-line-length
                            annotName: annotation.AnnotName, comments: this.pdfViewer.annotationModule.getAnnotationComments(annotation.Comments, annotation, annotation.Author), review: { state: annotation.State, stateModel: annotation.StateModel, modifiedDate: annotation.ModifiedDate, author: annotation.Author }, shapeAnnotationType: 'textMarkup'
                        };
                        this.pdfViewer.annotationModule.storeAnnotations(pageNumber, annotationObject, '_annotations_textMarkup');
                    }
                    // tslint:disable-next-line:max-line-length
                    const type: string = annotation.TextMarkupAnnotationType ? annotation.TextMarkupAnnotationType : annotation.textMarkupAnnotationType;
                    // tslint:disable-next-line
                    let annotBounds: any = annotation.Bounds ? annotation.Bounds : annotation.bounds;
                    const opacity: number = annotation.Opacity ? annotation.Opacity : annotation.opacity;
                    const color: string = annotation.Color ? annotation.Color : annotation.color;
                    switch (type) {
                        case 'Highlight':
                            this.renderHighlightAnnotation(annotBounds, opacity, color, context, factor);
                            break;
                        case 'Strikethrough':
                            this.renderStrikeoutAnnotation(annotBounds, opacity, color, context, factor);
                            break;
                        case 'Underline':
                            this.renderUnderlineAnnotation(annotBounds, opacity, color, context, factor);
                            break;
                    }
                }
            }
            if (pageNumber === this.selectTextMarkupCurrentPage) {
                if (!this.isAnnotationSelect) {
                    this.maintainAnnotationSelection();
                } else {
                    this.isAnnotationSelect = false;
                }
            }
        }
    }

    /**
     * @private
     */
    public drawTextMarkupAnnotations(type: string): void {
        this.isTextMarkupAnnotationMode = true;
        this.currentTextMarkupAddMode = type;
        const selectionObject: ISelection[] = this.pdfViewer.textSelectionModule.selectionRangeArray;
        if (selectionObject.length > 0 && !this.isSelectionMaintained) {
            this.convertSelectionToTextMarkup(type, selectionObject, this.pdfViewerBase.getZoomFactor());
        }
        if (this.pdfViewer.enableTextMarkupResizer && this.isExtended && window.getSelection().toString()) {
            const pageBounds: IPageAnnotationBounds[] = this.getDrawnBounds();
            if (pageBounds[0] && pageBounds[0].bounds) {
                this.updateTextMarkupAnnotationBounds(pageBounds[0].bounds);
            }

        } else if (window.getSelection().toString()) {
            const pageBounds: IPageAnnotationBounds[] = this.getDrawnBounds();
            if (pageBounds.length > 0) {
                for (let i = 0; i < pageBounds.length; i++) {
                    // tslint:disable-next-line:max-line-length
                    this.drawTextMarkups(type, pageBounds[i].bounds, pageBounds[i].pageIndex, pageBounds[i].rect, this.pdfViewerBase.getZoomFactor(), pageBounds[i].textContent, pageBounds[i].startIndex, pageBounds[i].endIndex);
                }
            }
        }
        this.isExtended = false;
        this.isSelectionMaintained = false;
        // this.pdfViewerBase.annotationHelper.redoCollection = [];
        this.pdfViewer.textSelectionModule.clearTextSelection();
        if (this.pdfViewer.enableTextMarkupResizer) {
            this.updateAnnotationBounds();
        }
    }

    private convertSelectionToTextMarkup(type: string, selectionObject: ISelection[], factor: number): void {
        for (let i = 0; i < selectionObject.length; i++) {
            const textValue: string = selectionObject[i].textContent.replace(/(\r\n|\n|\r)/gm, '');
            // tslint:disable-next-line
            let indexes: any;
            if (selectionObject[i].startNode === selectionObject[i].endNode) {
                const parentText: string = document.getElementById(selectionObject[i].startNode).textContent.replace(/(\r\n|\n|\r)/gm, '');
                indexes = this.getIndexNumbers(selectionObject[i].pageNumber, textValue, parentText);
            } else {
                indexes = this.getIndexNumbers(selectionObject[i].pageNumber, textValue);
            }
            // tslint:disable-next-line:max-line-length
            this.drawTextMarkups(type, selectionObject[i].rectangleBounds, selectionObject[i].pageNumber, selectionObject[i].bound, factor, textValue, indexes.startIndex, indexes.endIndex);
        }
    }

    // tslint:disable-next-line
    private updateTextMarkupAnnotationBounds(currentBounds: any): void {
        if (this.currentTextMarkupAnnotation) {
            const pageAnnotations: ITextMarkupAnnotation[] = this.getAnnotations(this.selectTextMarkupCurrentPage, null);
            const annotation: ITextMarkupAnnotation = null;
            if (pageAnnotations) {
                for (let i = 0; i < pageAnnotations.length; i++) {
                    if (JSON.stringify(this.currentTextMarkupAnnotation) === JSON.stringify(pageAnnotations[i])) {
                        pageAnnotations[i].bounds = currentBounds;
                    }
                }
                this.manageAnnotations(pageAnnotations, this.selectTextMarkupCurrentPage);
                this.currentTextMarkupAnnotation = null;
                this.pdfViewer.annotationModule.renderAnnotations(this.selectTextMarkupCurrentPage, null, null, null);
                this.pdfViewerBase.isDocumentEdited = true;
                // tslint:disable-next-line:max-line-length
                this.currentAnnotationIndex = null;
                this.selectTextMarkupCurrentPage = null;
            }
        }
    }

    // tslint:disable-next-line
    private drawTextMarkups(type: string, bounds: IRectangle[], pageNumber: number, rect: any, factor: number, textContent: string, startIndex: number, endIndex: number): void {
        let annotation: ITextMarkupAnnotation = null;
        const context: CanvasRenderingContext2D = this.getPageContext(pageNumber);
        if (context) {
            context.setLineDash([]);
            switch (type) {
                case 'Highlight':
                    // tslint:disable-next-line:max-line-length
                    annotation = this.getAddedAnnotation(type, this.highlightColor, this.highlightOpacity, bounds, this.pdfViewer.highlightSettings.author, this.pdfViewer.highlightSettings.subject, this.pdfViewer.highlightSettings.modifiedDate, '', rect, pageNumber);
                    if (annotation) {
                        this.renderHighlightAnnotation(annotation.bounds, annotation.opacity, annotation.color, context, factor);
                    }
                    break;
                case 'Strikethrough':
                    // tslint:disable-next-line:max-line-length
                    annotation = this.getAddedAnnotation(type, this.strikethroughColor, this.strikethroughOpacity, bounds, this.pdfViewer.strikethroughSettings.author, this.pdfViewer.strikethroughSettings.subject, this.pdfViewer.strikethroughSettings.modifiedDate, '', rect, pageNumber);
                    if (annotation) {
                        this.renderStrikeoutAnnotation(annotation.bounds, annotation.opacity, annotation.color, context, factor);
                    }
                    break;
                case 'Underline':
                    // tslint:disable-next-line:max-line-length
                    annotation = this.getAddedAnnotation(type, this.underlineColor, this.underlineOpacity, bounds, this.pdfViewer.underlineSettings.author, this.pdfViewer.underlineSettings.subject, this.pdfViewer.underlineSettings.modifiedDate, '', rect, pageNumber);
                    if (annotation) {
                        this.renderUnderlineAnnotation(annotation.bounds, annotation.opacity, annotation.color, context, factor);
                    }
                    break;
            }
            if (annotation) {
                this.pdfViewerBase.isDocumentEdited = true;
                // tslint:disable-next-line
                let settings: any = { opacity: annotation.opacity, color: annotation.color, author: annotation.author, subject: annotation.subject, modifiedDate: annotation.modifiedDate };
                const index: number = this.pdfViewer.annotationModule.actionCollection[this.pdfViewer.annotationModule.actionCollection.length - 1].index;
                // tslint:disable-next-line:max-line-length
                this.pdfViewer.fireAnnotationAdd(pageNumber, annotation.annotName, (type as AnnotationType), annotation.bounds, settings, textContent, startIndex, endIndex);
            }
        }
    }

    // tslint:disable-next-line
    // context:CanvasRenderingContext2D
    public renderHighlightAnnotation(bounds: any[], opacity: number, color: string, context: any, factor: number): void {
        for (let i = 0; i < bounds.length; i++) {
            // tslint:disable-next-line
            let bound: any = bounds[i];
            context.beginPath();
            const x: number = bound.X ? bound.X : bound.left;
            const y: number = bound.Y ? bound.Y : bound.top;
            const width: number = bound.Width ? bound.Width : bound.width;
            const height: number = bound.Height ? bound.Height : bound.height;
            // tslint:disable-next-line:max-line-length
            context.rect((x * factor), (y * factor), (width * factor), (height * factor));
            context.globalAlpha = opacity * 0.5;
            context.closePath();
            context.fillStyle = color;
            context.msFillRule = 'nonzero';
            context.fill();
        }
        context.save();
    }

    // tslint:disable-next-line
    private renderStrikeoutAnnotation(bounds: any[], opacity: number, color: string, context: CanvasRenderingContext2D, factor: number): void {
        for (let i = 0; i < bounds.length; i++) {
            // tslint:disable-next-line
            let bound: any = this.getProperBounds(bounds[i]);
            this.drawLine(opacity, bound.x, bound.y, bound.width, (bound.height / 2), color, factor, context);
        }
    }

    // tslint:disable-next-line
    private renderUnderlineAnnotation(bounds: any[], opacity: number, color: string, context: CanvasRenderingContext2D, factor: number): void {
        for (let i = 0; i < bounds.length; i++) {
            // tslint:disable-next-line
            let boundValues: any = this.getProperBounds(bounds[i]);
            this.drawLine(opacity, boundValues.x, boundValues.y, boundValues.width, boundValues.height, color, factor, context);
        }
    }

    // tslint:disable-next-line
    private getProperBounds(bound: any): any {
        const x: number = bound.X ? bound.X : bound.left;
        const y: number = bound.Y ? bound.Y : bound.top;
        const width: number = bound.Width ? bound.Width : bound.width;
        const height: number = bound.Height ? bound.Height : bound.height;
        return { x, y, width, height };
    }

    // tslint:disable-next-line:max-line-length
    private drawLine(opacity: number, x: number, y: number, width: number, height: number, color: string, factor: number, context: any): void {
        context.globalAlpha = opacity;
        context.beginPath();
        context.moveTo((x * factor), (y + height) * factor);
        context.lineTo((width + x) * factor, (y + height) * factor);
        context.lineWidth = 1;
        context.strokeStyle = color;
        context.closePath();
        context.msFillRule = 'nonzero';
        context.stroke();
    }

    /**
     * @private
     */
    // tslint:disable-next-line
    public printTextMarkupAnnotations(textMarkupAnnotations: any, pageIndex: number, stampData: any, shapeData: any, measureShapeData: any, stickyData: any): string {
        const canvas: HTMLCanvasElement = createElement('canvas', { id: this.pdfViewer.element.id + '_print_annotation_layer_' + pageIndex }) as HTMLCanvasElement;
        canvas.style.width = 816 + 'px';
        canvas.style.height = 1056 + 'px';
        const pageWidth: number = this.pdfViewerBase.pageSize[pageIndex].width;
        const pageHeight: number = this.pdfViewerBase.pageSize[pageIndex].height;
        canvas.height = pageHeight * this.pdfViewer.magnification.zoomFactor;
        canvas.width = pageWidth * this.pdfViewer.magnification.zoomFactor;
        // tslint:disable-next-line
        let textMarkupannotations: any = this.getAnnotations(pageIndex, null, '_annotations_textMarkup');
        // tslint:disable-next-line
        let shapeAnnotation: any = this.getAnnotations(pageIndex, null, '_annotations_shape');
        // tslint:disable-next-line
        let measureShapeAnnotation: any = this.getAnnotations(pageIndex, null, '_annotations_shape_measure');
        // tslint:disable-next-line
        let stampAnnotation: any = this.getAnnotations(pageIndex, null, '_annotations_stamp');
        // tslint:disable-next-line
        let stickyNoteAnnotation: any = this.getAnnotations(pageIndex, null, '_annotations_sticky');
        if (stampAnnotation || shapeAnnotation || stickyNoteAnnotation || measureShapeAnnotation) {
            this.pdfViewer.renderDrawing(canvas, pageIndex);
        } else {
            this.pdfViewer.annotation.renderAnnotations(pageIndex, shapeData, measureShapeData, null, canvas);
            this.pdfViewer.annotation.stampAnnotationModule.renderStampAnnotations(stampData, pageIndex, canvas);
            this.pdfViewer.annotation.stickyNotesAnnotationModule.renderStickyNotesAnnotations(stickyData, pageIndex, canvas);
        }
        if (textMarkupannotations) {
            this.renderTextMarkupAnnotations(null, pageIndex, canvas, 1);
        } else {
            this.renderTextMarkupAnnotations(textMarkupAnnotations, pageIndex, canvas, 1);
        }
        const imageSource: string = (canvas as HTMLCanvasElement).toDataURL();
        return imageSource;
    }

    /**
     * @private
     */
    public saveTextMarkupAnnotations(): string {
        // tslint:disable-next-line
        let storeTextMarkupObject: any = window.sessionStorage.getItem(this.pdfViewerBase.documentId + '_annotations_textMarkup');
        // tslint:disable-next-line
        let textMarkupAnnotations: Array<any> = new Array();
        const textMarkupColorpick: ColorPicker = new ColorPicker();
        for (let j = 0; j < this.pdfViewerBase.pageCount; j++) {
            textMarkupAnnotations[j] = [];
        }
        if (storeTextMarkupObject) {
            const textMarkupAnnotationCollection: IPageAnnotations[] = JSON.parse(storeTextMarkupObject);
            for (let i = 0; i < textMarkupAnnotationCollection.length; i++) {
                let newArray: ITextMarkupAnnotation[] = [];
                const pageAnnotationObject: IPageAnnotations = textMarkupAnnotationCollection[i];
                if (pageAnnotationObject) {
                    for (let z = 0; pageAnnotationObject.annotations.length > z; z++) {
                        // tslint:disable-next-line:max-line-length
                        pageAnnotationObject.annotations[z].bounds = JSON.stringify(this.getBoundsForSave(pageAnnotationObject.annotations[z].bounds));
                        const colorString: string = textMarkupColorpick.getValue(pageAnnotationObject.annotations[z].color, 'rgba');
                        pageAnnotationObject.annotations[z].color = JSON.stringify(this.getRgbCode(colorString));
                        pageAnnotationObject.annotations[z].rect = JSON.stringify(pageAnnotationObject.annotations[z].rect);
                    }
                    newArray = pageAnnotationObject.annotations;
                }
                textMarkupAnnotations[pageAnnotationObject.pageIndex] = newArray;
            }
        }
        return JSON.stringify(textMarkupAnnotations);
    }

    /**
     * @private
     */
    public deleteTextMarkupAnnotation(): void {
        if (this.currentTextMarkupAnnotation) {
            this.showHideDropletDiv(true);
            const pageAnnotations: ITextMarkupAnnotation[] = this.getAnnotations(this.selectTextMarkupCurrentPage, null);
            let deletedAnnotation: ITextMarkupAnnotation = null;
            if (pageAnnotations) {
                for (let i = 0; i < pageAnnotations.length; i++) {
                    if (JSON.stringify(this.currentTextMarkupAnnotation) === JSON.stringify(pageAnnotations[i])) {
                        deletedAnnotation = pageAnnotations.splice(i, 1)[0];
                        // tslint:disable-next-line:max-line-length
                        this.pdfViewer.annotationModule.addAction(this.selectTextMarkupCurrentPage, i, deletedAnnotation, 'Text Markup Deleted', null);
                        this.currentAnnotationIndex = i;
                        this.pdfViewer.annotation.stickyNotesAnnotationModule.findPosition(deletedAnnotation, 'textMarkup');
                        const removeDiv: HTMLElement = document.getElementById(deletedAnnotation.annotName);
                        if (removeDiv) {
                            if (removeDiv.parentElement.childElementCount === 1) {
                                this.pdfViewer.annotationModule.stickyNotesAnnotationModule.updateAccordionContainer(removeDiv);
                            } else {
                                removeDiv.remove();
                            }
                        }
                    }
                }
                this.manageAnnotations(pageAnnotations, this.selectTextMarkupCurrentPage);
                const annotationId: string = this.currentTextMarkupAnnotation.annotName;
                this.currentTextMarkupAnnotation = null;
                this.pdfViewer.annotationModule.renderAnnotations(this.selectTextMarkupCurrentPage, null, null, null);
                this.pdfViewerBase.isDocumentEdited = true;
                // tslint:disable-next-line:max-line-length
                this.pdfViewer.fireAnnotationRemove(this.selectTextMarkupCurrentPage, annotationId, deletedAnnotation.textMarkupAnnotationType as AnnotationType);
                this.currentAnnotationIndex = null;
                this.selectTextMarkupCurrentPage = null;
                if (Browser.isDevice) {
                    // tslint:disable-next-line:max-line-length
                    this.pdfViewer.toolbarModule.annotationToolbarModule.hideMobileAnnotationToolbar();
                    this.pdfViewer.toolbarModule.showToolbar(true);
                }
            }
        }
    }

    /**
     * @private
     */
    public modifyColorProperty(color: string): void {
        if (this.currentTextMarkupAnnotation) {
            const pageAnnotations: ITextMarkupAnnotation[] = this.modifyAnnotationProperty('Color', color, null);
            this.manageAnnotations(pageAnnotations, this.selectTextMarkupCurrentPage);
            this.pdfViewer.annotationModule.renderAnnotations(this.selectTextMarkupCurrentPage, null, null, null);
            this.pdfViewerBase.isDocumentEdited = true;
            // tslint:disable-next-line:max-line-length
            this.pdfViewer.fireAnnotationPropertiesChange(this.selectTextMarkupCurrentPage, this.currentTextMarkupAnnotation.annotName, this.currentTextMarkupAnnotation.textMarkupAnnotationType as AnnotationType, true, false, false, false);
            this.currentAnnotationIndex = null;
        }
    }

    /**
     * @private
     */
    public modifyOpacityProperty(args: ChangeEventArgs, isOpacity?: number): void {
        if (this.currentTextMarkupAnnotation) {
            let pageAnnotations: ITextMarkupAnnotation[];
            if (isOpacity) {
                pageAnnotations = this.modifyAnnotationProperty('Opacity', isOpacity, 'changed');
            } else {
                pageAnnotations = this.modifyAnnotationProperty('Opacity', args.value / 100, args.name);
            }
            if (pageAnnotations) {
                this.manageAnnotations(pageAnnotations, this.selectTextMarkupCurrentPage);
                this.pdfViewer.annotationModule.renderAnnotations(this.selectTextMarkupCurrentPage, null, null, null);
                if (isOpacity || args.name === 'changed') {
                    this.pdfViewerBase.isDocumentEdited = true;
                    // tslint:disable-next-line:max-line-length
                    this.pdfViewer.fireAnnotationPropertiesChange(this.selectTextMarkupCurrentPage, this.currentTextMarkupAnnotation.annotName, this.currentTextMarkupAnnotation.textMarkupAnnotationType as AnnotationType, false, true, false, false);
                    this.currentAnnotationIndex = null;
                }
            }
        }
    }


    // tslint:disable-next-line
    private modifyAnnotationProperty(property: string, value: any, status: string, annotName?: string): ITextMarkupAnnotation[] {
        const pageAnnotations: ITextMarkupAnnotation[] = this.getAnnotations(this.selectTextMarkupCurrentPage, null);
        if (pageAnnotations) {
            for (let i = 0; i < pageAnnotations.length; i++) {
                if (JSON.stringify(this.currentTextMarkupAnnotation) === JSON.stringify(pageAnnotations[i])) {
                    const date: Date = new Date();
                    if (property === 'Color') {
                        pageAnnotations[i].color = value;
                    } else {
                        pageAnnotations[i].opacity = value;
                    }
                    pageAnnotations[i].modifiedDate = date.toLocaleString();
                    this.currentAnnotationIndex = i;
                    if (status === null || status === 'changed') {
                        // tslint:disable-next-line:max-line-length
                        this.pdfViewer.annotationModule.addAction(this.selectTextMarkupCurrentPage, i, this.currentTextMarkupAnnotation, 'Text Markup Property modified', property);
                    }
                    this.currentTextMarkupAnnotation = pageAnnotations[i];
                    this.pdfViewer.annotationModule.stickyNotesAnnotationModule.updateAnnotationModifiedDate(pageAnnotations[i]);
                }
            }
        }
        return pageAnnotations;
    }

    /**
     * @private
     */
    public undoTextMarkupAction(annotation: ITextMarkupAnnotation, pageNumber: number, index: number, action: string): void {
        const pageAnnotations: ITextMarkupAnnotation[] = this.getAnnotations(pageNumber, null);
        if (pageAnnotations) {
            if (action === 'Text Markup Added') {
                this.pdfViewer.annotation.stickyNotesAnnotationModule.findPosition(pageAnnotations[index], 'textMarkup');
                // tslint:disable-next-line
                let removeDiv: any = document.getElementById(pageAnnotations[index].annotName);
                if (removeDiv) {
                    if (removeDiv.parentElement.childElementCount === 1) {
                        this.pdfViewer.annotationModule.stickyNotesAnnotationModule.updateAccordionContainer(removeDiv);
                    } else {
                        removeDiv.remove();
                    }
                }
                pageAnnotations.splice(index, 1);
            } else if (action === 'Text Markup Deleted') {
                // tslint:disable-next-line:max-line-length
                this.pdfViewer.annotationModule.stickyNotesAnnotationModule.addAnnotationComments(pageNumber, annotation.shapeAnnotationType);
                pageAnnotations.splice(index, 0, annotation);
            }
        }
        this.clearCurrentAnnotation();
        this.pdfViewerBase.isDocumentEdited = true;
        this.manageAnnotations(pageAnnotations, pageNumber);
        this.pdfViewer.annotationModule.renderAnnotations(pageNumber, null, null, null);
    }

    /**
     * @private
     */
    // tslint:disable-next-line:max-line-length
    public undoRedoPropertyChange(annotation: ITextMarkupAnnotation, pageNumber: number, index: number, property: string, isUndoAction?: boolean): ITextMarkupAnnotation {
        const pageAnnotations: ITextMarkupAnnotation[] = this.getAnnotations(pageNumber, null);
        if (pageAnnotations) {
            if (property === 'Color') {
                const tempColor: string = pageAnnotations[index].color;
                pageAnnotations[index].color = annotation.color;
                annotation.color = tempColor;
            } else {
                const tempOpacity: number = pageAnnotations[index].opacity;
                pageAnnotations[index].opacity = annotation.opacity;
                annotation.opacity = tempOpacity;
            }
            this.pdfViewer.annotationModule.stickyNotesAnnotationModule.updateAnnotationModifiedDate(annotation, null, true);
            if (isUndoAction) {
                annotation.modifiedDate = new Date().toLocaleString();
            }
        }
        this.clearCurrentAnnotation();
        this.pdfViewerBase.isDocumentEdited = true;
        this.manageAnnotations(pageAnnotations, pageNumber);
        this.pdfViewer.annotationModule.renderAnnotations(pageNumber, null, null, null);
        return annotation;
    }

    /**
     * @private
     */
    public redoTextMarkupAction(annotation: ITextMarkupAnnotation, pageNumber: number, index: number, action: string): void {
        const pageAnnotations: ITextMarkupAnnotation[] = this.getAnnotations(pageNumber, null);
        if (pageAnnotations) {
            if (action === 'Text Markup Added') {
                // tslint:disable-next-line:max-line-length
                this.pdfViewer.annotationModule.stickyNotesAnnotationModule.addAnnotationComments(pageNumber, annotation.shapeAnnotationType);
                pageAnnotations.push(annotation);
            } else if (action === 'Text Markup Deleted') {
                this.pdfViewer.annotation.stickyNotesAnnotationModule.findPosition(pageAnnotations[index], 'textMarkup');
                // tslint:disable-next-line
                let removeDiv: any = document.getElementById(pageAnnotations[index].annotName);
                if (removeDiv) {
                    if (removeDiv.parentElement.childElementCount === 1) {
                        this.pdfViewer.annotationModule.stickyNotesAnnotationModule.updateAccordionContainer(removeDiv);
                    } else {
                        removeDiv.remove();
                    }
                }
                pageAnnotations.splice(index, 1);
            }
        }
        this.clearCurrentAnnotation();
        this.pdfViewerBase.isDocumentEdited = true;
        this.manageAnnotations(pageAnnotations, pageNumber);
        this.pdfViewer.annotationModule.renderAnnotations(pageNumber, null, null, null);
    }

    /**
     * @private
     */
    public saveNoteContent(pageNumber: number, note: string): void {
        const pageAnnotations: ITextMarkupAnnotation[] = this.getAnnotations(pageNumber, null);
        if (pageAnnotations) {
            for (let i = 0; i < pageAnnotations.length; i++) {
                if (JSON.stringify(this.currentTextMarkupAnnotation) === JSON.stringify(pageAnnotations[i])) {
                    pageAnnotations[i].note = note;
                }
            }
        }
        this.manageAnnotations(pageAnnotations, pageNumber);
        this.pdfViewerBase.isDocumentEdited = true;
    }

    private clearCurrentAnnotation(): void {
        if (!this.isExtended) {
            this.selectTextMarkupCurrentPage = null;
            this.currentTextMarkupAnnotation = null;
            let isSkip = false;
            // tslint:disable-next-line:max-line-length
            if (this.pdfViewer.annotation.freeTextAnnotationModule && this.pdfViewer.annotation.freeTextAnnotationModule.isInuptBoxInFocus) {
                isSkip = true;
            }
            if (!isSkip) {
                this.enableAnnotationPropertiesTool(false);
            }
        }
    }

    /**
     * @private
     */
    public clearCurrentAnnotationSelection(pageNumber: number, isSelect?: boolean): void {
        if (isSelect) {
            this.isAnnotationSelect = true;
        } else {
            this.isAnnotationSelect = false;
        }
        const lowerPageIndex: number = (pageNumber - 2) >= 0 ? (pageNumber - 2) : 0;
        // tslint:disable-next-line:max-line-length
        const higherPageIndex: number = (pageNumber + 2) < this.pdfViewerBase.pageCount ? (pageNumber + 2) : this.pdfViewerBase.pageCount - 1;
        for (let k: number = lowerPageIndex; k <= higherPageIndex; k++) {
            this.clearAnnotationSelection(k);
        }
    }

    // tslint:disable-next-line
    private getBoundsForSave(bounds: any): any {
        // tslint:disable-next-line
        let newArray: any[] = [];
        for (let i = 0; i < bounds.length; i++) {
            const left: number = bounds[i].left ? bounds[i].left : bounds[i].Left;
            const top: number = bounds[i].top ? bounds[i].top : bounds[i].Top;
            const height: number = bounds[i].height ? bounds[i].height : bounds[i].Height;
            const width: number = bounds[i].width ? bounds[i].width : bounds[i].Width;
            newArray.push({ left, top, width, height });
        }
        return newArray;
    }

    // tslint:disable-next-line
    private getRgbCode(colorString: string): any {
        const markupStringArray: string[] = colorString.split(',');
        // tslint:disable-next-line:radix
        const textMarkupR: number = parseInt(markupStringArray[0].split('(')[1]);
        // tslint:disable-next-line:radix
        const textMarkupG: number = parseInt(markupStringArray[1]);
        // tslint:disable-next-line:radix
        const textMarkupB: number = parseInt(markupStringArray[2]);
        // tslint:disable-next-line:radix
        const textMarkupA: number = parseInt(markupStringArray[3]);
        return { a: textMarkupA * 255, r: textMarkupR, g: textMarkupG, b: textMarkupB };
    }

    private getDrawnBounds(): IPageAnnotationBounds[] {
        const pageBounds: IPageAnnotationBounds[] = [];
        const selection: Selection = window.getSelection();
        if (selection.anchorNode !== null) {
            const range: Range = document.createRange();
            const isBackWardSelection: boolean = this.pdfViewerBase.textLayer.isBackWardSelection(selection);
            if (selection.anchorNode === selection.focusNode) {
                const pageId: number = this.pdfViewerBase.textLayer.getPageIndex(selection.anchorNode);
                let startIndex = 0;
                let endIndex = 0;
                if (!isNaN(pageId)) {
                    const pageRect: ClientRect = this.pdfViewerBase.getElement('_pageDiv_' + pageId).getBoundingClientRect();
                    if (isBackWardSelection) {
                        range.setStart(selection.focusNode, selection.focusOffset);
                        range.setEnd(selection.anchorNode, selection.anchorOffset);
                    } else {
                        if (selection.anchorOffset < selection.focusOffset) {
                            startIndex = selection.anchorOffset;
                            endIndex = selection.focusOffset;
                            range.setStart(selection.anchorNode, selection.anchorOffset);
                            range.setEnd(selection.focusNode, selection.focusOffset);
                        } else {
                            startIndex = selection.focusOffset;
                            endIndex = selection.anchorOffset;
                            range.setStart(selection.focusNode, selection.focusOffset);
                            range.setEnd(selection.anchorNode, selection.anchorOffset);
                        }
                    }
                    const boundingRect: ClientRect = range.getBoundingClientRect();
                    // tslint:disable-next-line
                    let indexes: any = this.getIndexNumbers(pageId, range.toString(), range.commonAncestorContainer.textContent.toString().replace(/(\r\n|\n|\r)/gm, ''));
                    // tslint:disable-next-line:max-line-length
                    const rectangle: IRectangle = { left: this.getDefaultValue(boundingRect.left - pageRect.left), top: this.getDefaultValue(boundingRect.top - pageRect.top), width: this.getDefaultValue(boundingRect.width), height: this.getDefaultValue(boundingRect.height), right: this.getDefaultValue(boundingRect.right - pageRect.left), bottom: this.getDefaultValue(boundingRect.bottom - pageRect.top) };
                    const rectangleArray: IRectangle[] = [];
                    rectangleArray.push(rectangle);
                    // tslint:disable-next-line
                    let rect: any = { left: rectangle.left, top: rectangle.top, right: rectangle.right, bottom: rectangle.bottom };
                    pageBounds.push({ pageIndex: pageId, bounds: rectangleArray, rect, startIndex: indexes.startIndex, endIndex: indexes.endIndex, textContent: range.toString() });
                }
            } else {
                let startNode: Node; let endNode: Node;
                let selectionStartOffset: number; let selectionEndOffset: number;
                if (isBackWardSelection) {
                    startNode = selection.focusNode;
                    selectionStartOffset = selection.focusOffset;
                    endNode = selection.anchorNode;
                    selectionEndOffset = selection.anchorOffset;
                } else {
                    startNode = selection.anchorNode;
                    selectionStartOffset = selection.anchorOffset;
                    endNode = selection.focusNode;
                    selectionEndOffset = selection.focusOffset;
                }
                const anchorPageId: number = this.pdfViewerBase.textLayer.getPageIndex(startNode);
                const anchorTextId: number = this.pdfViewerBase.textLayer.getTextIndex(startNode, anchorPageId);
                const focusPageId: number = this.pdfViewerBase.textLayer.getPageIndex(endNode);
                const focusTextId: number = this.pdfViewerBase.textLayer.getTextIndex(endNode, focusPageId);
                let startOffset = 0; let endOffset = 0; let currentId = 0;
                for (let i: number = anchorPageId; i <= focusPageId; i++) {
                    const selectionRects: IRectangle[] = [];
                    let pageStartId: number; let pageEndId: number; let pageStartOffset: number; let pageEndOffset: number;
                    const textDivs: NodeList = this.pdfViewerBase.getElement('_textLayer_' + i).childNodes;
                    const pageRect: ClientRect = this.pdfViewerBase.getElement('_pageDiv_' + i).getBoundingClientRect();
                    if (i === anchorPageId) {
                        currentId = anchorTextId;
                    } else {
                        currentId = 0;
                    }
                    for (let j: number = currentId; j < textDivs.length; j++) {
                        const textElement: HTMLElement = textDivs[j] as HTMLElement;
                        if (j === currentId) {
                            pageStartId = currentId;
                            pageStartOffset = (i === anchorPageId) ? selectionStartOffset : 0;
                        } else {
                            pageEndId = j;
                            pageEndOffset = (i === focusPageId) ? selectionEndOffset : textElement.textContent.length;
                        }
                        if (j === anchorTextId && i === anchorPageId) {
                            startOffset = selectionStartOffset;
                        } else {
                            startOffset = 0;
                        }
                        if (j === focusTextId && i === focusPageId) {
                            endOffset = selectionEndOffset;
                        } else {
                            endOffset = textElement.textContent.length;
                        }
                        for (let k = 0; k < textElement.childNodes.length; k++) {
                            const node: Node = textElement.childNodes[k];
                            range.setStart(node, startOffset);
                            range.setEnd(node, endOffset);
                        }
                        const boundingRect: ClientRect = range.getBoundingClientRect();
                        // tslint:disable-next-line:max-line-length
                        const rectangle: IRectangle = { left: this.getDefaultValue(boundingRect.left - pageRect.left), top: this.getDefaultValue(boundingRect.top - pageRect.top), width: this.getDefaultValue(boundingRect.width), height: this.getDefaultValue(boundingRect.height), right: this.getDefaultValue(boundingRect.right - pageRect.left), bottom: this.getDefaultValue(boundingRect.bottom - pageRect.top) };
                        selectionRects.push(rectangle);
                        range.detach();
                        if (i === focusPageId && j === focusTextId) {
                            break;
                        }
                    }
                    const startElementNode: Node = this.pdfViewerBase.getElement('_text_' + i + '_' + pageStartId).childNodes[0];
                    const endElementNode: Node = this.pdfViewerBase.getElement('_text_' + i + '_' + pageEndId).childNodes[0];
                    const pageRange: Range = document.createRange();
                    pageRange.setStart(startElementNode, pageStartOffset);
                    pageRange.setEnd(endElementNode, pageEndOffset);
                    const pageRectBounds: ClientRect = pageRange.getBoundingClientRect();
                    const textValue: string = pageRange.toString().replace(/(\r\n|\n|\r)/gm, '');
                    // tslint:disable-next-line
                    let indexes: any = this.getIndexNumbers(i, textValue);
                    // tslint:disable-next-line:max-line-length
                    const pageRectangle: IRectangle = { left: this.getDefaultValue(pageRectBounds.left - pageRect.left), top: this.getDefaultValue(pageRectBounds.top - pageRect.top), width: this.getDefaultValue(pageRectBounds.width), height: this.getDefaultValue(pageRectBounds.height), right: this.getDefaultValue(pageRectBounds.right - pageRect.left), bottom: this.getDefaultValue(pageRectBounds.bottom - pageRect.top) };
                    // tslint:disable-next-line
                    let rect: any = { left: pageRectangle.left, top: pageRectangle.top, right: pageRectangle.right, bottom: pageRectangle.bottom };
                    pageBounds.push({ pageIndex: i, bounds: selectionRects, rect, startIndex: indexes.startIndex, endIndex: indexes.endIndex, textContent: textValue });
                }
            }
        }
        selection.removeAllRanges();
        return pageBounds;
    }

    // tslint:disable-next-line
    private getIndexNumbers(pageNumber: number, content: string, parentText?: string): any {
        // tslint:disable-next-line
        let storedData: any = this.pdfViewerBase.getStoredData(pageNumber);
        let startIndex: number;
        let endIndex: number;
        if (storedData) {
            const pageText: string = storedData.pageText.replace(/(\r\n|\n|\r)/gm, '');
            if (!isNullOrUndefined(parentText)) {
                const parentIndex: number = pageText.indexOf(parentText);
                const initialIndex: number = parentText.indexOf(content);
                startIndex = parentIndex + initialIndex;
            } else {
                startIndex = pageText.indexOf(content);
            }
            endIndex = startIndex + (content.length - 1);
        }
        return { startIndex, endIndex };
    }

    /**
     * @private
     */
    public rerenderAnnotationsPinch(pageNumber: number): void {
        let annotCanvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageNumber);
        if (annotCanvas) {
            const oldAnnotCanvas: HTMLElement = this.pdfViewerBase.getElement('_old_annotationCanvas_' + pageNumber);
            if (oldAnnotCanvas) {
                if (annotCanvas) {
                    oldAnnotCanvas.id = annotCanvas.id;
                    annotCanvas.parentElement.removeChild(annotCanvas);
                } else {
                    oldAnnotCanvas.id = this.pdfViewer.element.id + '_annotationCanvas_' + pageNumber;
                }
                annotCanvas = oldAnnotCanvas;
            }
            annotCanvas.style.width = '';
            annotCanvas.style.height = '';
            (annotCanvas as HTMLCanvasElement).width = this.pdfViewerBase.pageSize[pageNumber].width * this.pdfViewerBase.getZoomFactor();
            (annotCanvas as HTMLCanvasElement).height = this.pdfViewerBase.pageSize[pageNumber].height * this.pdfViewerBase.getZoomFactor();
            this.renderTextMarkupAnnotations(null, pageNumber, annotCanvas, this.pdfViewerBase.getZoomFactor());
        }
    }

    /**
     * @private
     */
    public rerenderAnnotations(pageNumber: number): void {
        const oldCanvas: HTMLElement = this.pdfViewerBase.getElement('_old_annotationCanvas_' + pageNumber);
        if (oldCanvas) {
            oldCanvas.parentElement.removeChild(oldCanvas);
        }
        const newCanvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageNumber);
        if (newCanvas) {
            newCanvas.style.display = 'block';
        }
    }

    /**
     * @private
     */
    public onTextMarkupAnnotationMouseUp(event: MouseEvent): void {
        const pageNumber: number = this.pdfViewer.annotationModule.getEventPageNumber(event);
        if (!isNullOrUndefined(pageNumber) && !isNaN(pageNumber)) {
            const canvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageNumber);
            this.clearCurrentSelectedAnnotation();
            const currentAnnot: ITextMarkupAnnotation = this.getCurrentMarkupAnnotation(event.clientX, event.clientY, pageNumber, canvas);
            if (currentAnnot) {
                this.selectAnnotation(currentAnnot, canvas, pageNumber);
                this.currentTextMarkupAnnotation = currentAnnot;
                this.selectTextMarkupCurrentPage = pageNumber;
                this.enableAnnotationPropertiesTool(true);
                const commentPanelDiv: HTMLElement = document.getElementById(this.pdfViewer.element.id + '_commantPanel');
                if (commentPanelDiv && commentPanelDiv.style.display === 'block') {
                    // tslint:disable-next-line
                    let accordionExpand: any = document.getElementById(this.pdfViewer.element.id + '_accordionContainer' + (pageNumber + 1));
                    if (accordionExpand) {
                        accordionExpand.ej2_instances[0].expandItem(true);
                    }
                    // tslint:disable-next-line
                    let comments: any = document.getElementById(currentAnnot.annotName);
                    if (comments) {
                        comments.firstChild.click();
                    }
                }
                if (this.pdfViewer.toolbarModule) {
                    this.pdfViewer.toolbarModule.annotationToolbarModule.isToolbarHidden = true;
                    this.pdfViewer.toolbarModule.annotationToolbarModule.showAnnotationToolbar(this.pdfViewer.toolbarModule.annotationItem);
                }
            } else {
                this.clearCurrentAnnotation();
            }
            this.clearCurrentAnnotationSelection(pageNumber);
        } else {
            if (!this.pdfViewerBase.isClickedOnScrollBar(event, true)) {
                this.clearCurrentAnnotation();
                this.clearCurrentAnnotationSelection(pageNumber);
            }
        }
    }

    /**
     * @private
     */
    public onTextMarkupAnnotationTouchEnd(event: TouchEvent): void {
        const pageNumber: number = this.pdfViewer.annotationModule.getEventPageNumber(event);
        if (!isNullOrUndefined(pageNumber) && !isNaN(pageNumber)) {
            this.clearCurrentAnnotationSelection(pageNumber);
            const touchCanvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageNumber);
            this.clearCurrentSelectedAnnotation();
            // tslint:disable-next-line:max-line-length
            const currentAnnot: ITextMarkupAnnotation = this.getCurrentMarkupAnnotation(event.touches[0].clientX, event.touches[0].clientY, pageNumber, touchCanvas);
            if (currentAnnot) {
                this.selectAnnotation(currentAnnot, touchCanvas, pageNumber);
                this.currentTextMarkupAnnotation = currentAnnot;
                this.selectTextMarkupCurrentPage = pageNumber;
                this.enableAnnotationPropertiesTool(true);
                // tslint:disable-next-line
                let accordionExpand: any = document.getElementById(this.pdfViewer.element.id + '_accordionContainer' + (pageNumber + 1));
                if (accordionExpand) {
                    accordionExpand.ej2_instances[0].expandItem(true);
                }
                // tslint:disable-next-line
                let comments: any = document.getElementById(currentAnnot.annotName);
                if (comments) {
                    comments.firstChild.click();
                }
            } else {
                this.clearCurrentAnnotation();
            }
            this.clearCurrentAnnotationSelection(pageNumber);
        } else if (this.selectTextMarkupCurrentPage != null && Browser.isDevice) {
            const number: number = this.selectTextMarkupCurrentPage;
            this.selectTextMarkupCurrentPage = null;
            this.clearAnnotationSelection(number);
        } else {
            this.clearCurrentAnnotation();
            this.clearCurrentAnnotationSelection(pageNumber);
        }
    }

    private clearCurrentSelectedAnnotation(): void {
        if (this.currentTextMarkupAnnotation) {
            this.clearAnnotationSelection(this.selectTextMarkupCurrentPage);
            this.clearCurrentAnnotation();
        }
    }

    /**
     * @private
     */
    public onTextMarkupAnnotationMouseMove(event: MouseEvent): void {
        const eventTarget: HTMLElement = event.target as HTMLElement;
        // tslint:disable-next-line
        let pageIndex: number = parseInt(eventTarget.id.split('_text_')[1]) || parseInt(eventTarget.id.split('_textLayer_')[1]) || parseInt(eventTarget.id.split('_annotationCanvas_')[1]);
        if (pageIndex) {
            const canvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageIndex);
            const currentAnnot: ITextMarkupAnnotation = this.getCurrentMarkupAnnotation(event.clientX, event.clientY, pageIndex, canvas);
            if (currentAnnot) {
                eventTarget.style.cursor = 'pointer';
                // this.showPopupNote(event, currentAnnot);
            } else {
                this.pdfViewer.annotationModule.hidePopupNote();
                if (this.pdfViewerBase.isPanMode && !this.pdfViewerBase.getAnnotationToolStatus()) {
                    eventTarget.style.cursor = 'grab';
                } else {
                    eventTarget.style.cursor = 'auto';
                }
            }
        }
    }

    // tslint:disable-next-line
    private showPopupNote(event: any, annotation: ITextMarkupAnnotation): void {
        if (annotation.note) {
            // tslint:disable-next-line:max-line-length
            this.pdfViewer.annotationModule.showPopupNote(event, annotation.color, annotation.author, annotation.note, annotation.textMarkupAnnotationType);
        }
    }

    private getCurrentMarkupAnnotation(clientX: number, clientY: number, pageNumber: number, canvas: HTMLElement): ITextMarkupAnnotation {
        const currentTextMarkupAnnotations: ITextMarkupAnnotation[] = [];
        const canvasParentPosition: ClientRect = canvas.parentElement.getBoundingClientRect();
        const leftClickPosition: number = clientX - canvasParentPosition.left;
        const topClickPosition: number = clientY - canvasParentPosition.top;
        const annotationList: ITextMarkupAnnotation[] = this.getAnnotations(pageNumber, null);
        let isAnnotationGot = false;
        if (annotationList) {
            for (let i = 0; i < annotationList.length; i++) {
                const annotation: ITextMarkupAnnotation = annotationList[i];
                for (let j = 0; j < annotation.bounds.length; j++) {
                    // tslint:disable-next-line
                    let bound: any = annotation.bounds[j];
                    const left: number = bound.left ? bound.left : bound.Left;
                    const top: number = bound.top ? bound.top : bound.Top;
                    const width: number = bound.width ? bound.width : bound.Width;
                    const height: number = bound.height ? bound.height : bound.Height;
                    // tslint:disable-next-line:max-line-length
                    if (leftClickPosition >= this.getMagnifiedValue(left, this.pdfViewerBase.getZoomFactor()) && leftClickPosition <= this.getMagnifiedValue(left + width, this.pdfViewerBase.getZoomFactor()) && topClickPosition >= this.getMagnifiedValue(top, this.pdfViewerBase.getZoomFactor()) && topClickPosition <= this.getMagnifiedValue(top + height, this.pdfViewerBase.getZoomFactor())) {
                        currentTextMarkupAnnotations.push(annotation);
                        isAnnotationGot = true;
                    } else {
                        if (isAnnotationGot) {
                            isAnnotationGot = false;
                            break;
                        }
                    }
                }
            }
        }
        let currentAnnot: ITextMarkupAnnotation = null;
        if (currentTextMarkupAnnotations.length > 1) {
            currentAnnot = this.compareCurrentAnnotations(currentTextMarkupAnnotations);
        } else if (currentTextMarkupAnnotations.length === 1) {
            currentAnnot = currentTextMarkupAnnotations[0];
        }
        return currentAnnot;
    }

    private compareCurrentAnnotations(annotations: ITextMarkupAnnotation[]): ITextMarkupAnnotation {
        let previousX: number;
        let currentAnnotation: ITextMarkupAnnotation = null;
        for (let i = 0; i < annotations.length; i++) {
            if (i === annotations.length - 1) {
                break;
            }
            // tslint:disable-next-line
            let firstAnnotBounds: any = annotations[i].bounds;
            const firstXposition: number = firstAnnotBounds[0].left ? firstAnnotBounds[0].left : firstAnnotBounds[0].Left;
            const firstYposition: number = firstAnnotBounds[0].top ? firstAnnotBounds[0].top : firstAnnotBounds[0].Top;
            // tslint:disable-next-line
            let secondAnnotBounds: any = annotations[i + 1].bounds;
            const secondXposition: number = secondAnnotBounds[0].left ? secondAnnotBounds[0].left : secondAnnotBounds[0].Left;
            const secondYposition: number = secondAnnotBounds[0].top ? secondAnnotBounds[0].top : secondAnnotBounds[0].Top;
            if ((firstXposition < secondXposition) || (firstYposition < secondYposition)) {
                previousX = secondXposition;
                currentAnnotation = annotations[i + 1];
            } else {
                previousX = firstXposition;
                currentAnnotation = annotations[i];
            }
            if (previousX && (i === (annotations.length - 2))) {
                if ((previousX === firstXposition) && (previousX === secondXposition)) {
                    previousX = secondXposition;
                    currentAnnotation = annotations[i + 1];
                }
            }
        }
        return currentAnnotation;
    }
    /**
     * @private
     */
    public clearAnnotationSelection(pageNumber: number): void {
        const canvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageNumber);
        if (canvas) {
            const context: CanvasRenderingContext2D = (canvas as HTMLCanvasElement).getContext('2d');
            context.setLineDash([]);
            this.pdfViewer.annotationModule.renderAnnotations(pageNumber, null, null, null);
        }
    }

    /**
     * @private
     */
    public selectAnnotation(annotation: ITextMarkupAnnotation, canvas: HTMLElement, pageNumber?: number): void {
        if (this.pdfViewer.selectedItems.annotations[0]) {
            this.pdfViewer.clearSelection(this.pdfViewer.selectedItems.annotations[0].pageIndex);
            this.pdfViewer.clearSelection(this.selectTextMarkupCurrentPage);
        }
        let isCurrentTextMarkup = false;
        if (!this.currentTextMarkupAnnotation) {
            isCurrentTextMarkup = true;
        }
        if (!isNaN(pageNumber)) {
            this.currentTextMarkupAnnotation = annotation;
            this.selectTextMarkupCurrentPage = pageNumber;
        }
        // tslint:disable-next-line
        let currentEvent: any = event;
        if (this.pdfViewer.enableTextMarkupResizer && annotation && currentEvent && !currentEvent.touches) {
            // tslint:disable-next-line
            let boundingRect: any = this.pdfViewerBase.getElement('_textLayer_' + this.selectTextMarkupCurrentPage).getBoundingClientRect();
            const left: number = annotation.bounds[0].left ? annotation.bounds[0].left : annotation.bounds[0].Left;
            const top: number = annotation.bounds[0].top ? annotation.bounds[0].top : annotation.bounds[0].Top;
            this.updateLeftposition(left * this.pdfViewerBase.getZoomFactor() + boundingRect.left, (boundingRect.top + top), true);
            // tslint:disable-next-line
            let endPosition: any = annotation.bounds[annotation.bounds.length - 1];
            const endLeft: number = endPosition.left ? endPosition.left : endPosition.Left;
            const endTop: number = endPosition.top ? endPosition.top : endPosition.Top;
            const endWidth: number = endPosition.width ? endPosition.width : endPosition.Width;
            // tslint:disable-next-line:max-line-length
            this.updatePosition((endLeft + endWidth) * this.pdfViewerBase.getZoomFactor() + boundingRect.left, (endTop + boundingRect.top), true);
        }
        for (let i = 0; i < annotation.bounds.length; i++) {
            // tslint:disable-next-line
            let bound: any = annotation.bounds[i];
            const x: number = bound.left ? bound.left : bound.Left;
            const y: number = bound.top ? bound.top : bound.Top;
            const width: number = bound.width ? bound.width : bound.Width;
            const height: number = bound.height ? bound.height : bound.Height;
            // tslint:disable-next-line:max-line-length
            this.drawAnnotationSelectRect(canvas, this.getMagnifiedValue(x - 0.5, this.pdfViewerBase.getZoomFactor()), this.getMagnifiedValue(y - 0.5, this.pdfViewerBase.getZoomFactor()), this.getMagnifiedValue(width + 0.5, this.pdfViewerBase.getZoomFactor()), this.getMagnifiedValue(height + 0.5, this.pdfViewerBase.getZoomFactor()));
        }
        if (annotation.annotName !== '') {
            if (isCurrentTextMarkup) {
                this.pdfViewer.annotationModule.selectAnnotation(annotation.annotName, this.selectTextMarkupCurrentPage, annotation);
            }
        }
        if (annotation && this.pdfViewer.enableTextMarkupResizer) {
            this.isTextMarkupAnnotationMode = true;
        }
    }

    private drawAnnotationSelectRect(canvas: HTMLElement, x: number, y: number, width: number, height: number): void {
        const context: CanvasRenderingContext2D = (canvas as HTMLCanvasElement).getContext('2d');
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.beginPath();
        context.setLineDash([4 * this.pdfViewerBase.getZoomFactor()]);
        context.globalAlpha = 1;
        context.rect(x, y, width, height);
        context.closePath();
        // tslint:disable-next-line:max-line-length
        const borderColor: string = isNullOrUndefined(this.pdfViewer.annotationSelectorSettings.selectionBorderColor) || this.pdfViewer.annotationSelectorSettings.selectionBorderColor === '' ? '#0000ff' : this.pdfViewer.annotationSelectorSettings.selectionBorderColor;
        context.strokeStyle = borderColor;
        // tslint:disable-next-line:max-line-length
        context.lineWidth = isNullOrUndefined(this.pdfViewer.annotationSelectorSettings.selectionBorderThickness) ? 1 : this.pdfViewer.annotationSelectorSettings.selectionBorderThickness;
        context.stroke();
        context.save();
    }

    /**
     * @private
     */
    public enableAnnotationPropertiesTool(isEnable: boolean): void {
        // tslint:disable-next-line:max-line-length
        if (this.pdfViewer.toolbarModule && this.pdfViewer.toolbarModule.annotationToolbarModule) {
            this.pdfViewer.toolbarModule.annotationToolbarModule.createMobileAnnotationToolbar(isEnable);
        }
        // tslint:disable-next-line:max-line-length
        if (this.pdfViewer.toolbarModule && this.pdfViewer.toolbarModule.annotationToolbarModule && this.pdfViewer.toolbarModule.annotationToolbarModule.isMobileAnnotEnabled && this.pdfViewer.selectedItems.annotations.length === 0) {
            if (this.pdfViewer.toolbarModule.annotationToolbarModule) {
                this.pdfViewer.toolbarModule.annotationToolbarModule.selectAnnotationDeleteItem(isEnable);
                let enable: boolean = isEnable;
                if (this.isTextMarkupAnnotationMode) {
                    enable = true;
                }
                this.pdfViewer.toolbarModule.annotationToolbarModule.enableTextMarkupAnnotationPropertiesTools(enable);
                if (this.currentTextMarkupAnnotation) {
                    // tslint:disable-next-line:max-line-length
                    this.pdfViewer.toolbarModule.annotationToolbarModule.updateColorInIcon(this.pdfViewer.toolbarModule.annotationToolbarModule.colorDropDownElement, this.currentTextMarkupAnnotation.color);
                } else {
                    if (!this.isTextMarkupAnnotationMode) {
                        // tslint:disable-next-line:max-line-length
                        this.pdfViewer.toolbarModule.annotationToolbarModule.updateColorInIcon(this.pdfViewer.toolbarModule.annotationToolbarModule.colorDropDownElement, '#000000');
                    } else {
                        this.pdfViewer.toolbarModule.annotationToolbarModule.setCurrentColorInPicker();
                    }
                }
            }
        }
    }

    /**
     * @private
     */
    public maintainAnnotationSelection(): void {
        if (this.currentTextMarkupAnnotation) {
            const canvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + this.selectTextMarkupCurrentPage);
            if (canvas) {
                this.selectAnnotation(this.currentTextMarkupAnnotation, canvas, this.selectTextMarkupCurrentPage);
            }
        }
    }

    // private storeAnnotations(pageNumber: number, annotation: ITextMarkupAnnotation): number {
    //     // tslint:disable-next-line
    //     let storeObject: any = window.sessionStorage.getItem(this.pdfViewerBase.documentId + '_annotations_textMarkup');
    //     let index: number = 0;
    //     if (!storeObject) {
    //         let markupAnnotation: IPageAnnotations = { pageIndex: pageNumber, annotations: [] };
    //         markupAnnotation.annotations.push(annotation);
    //         index = markupAnnotation.annotations.indexOf(annotation);
    //         let annotationCollection: IPageAnnotations[] = [];
    //         annotationCollection.push(markupAnnotation);
    //         let annotationStringified: string = JSON.stringify(annotationCollection);
    //         window.sessionStorage.setItem(this.pdfViewerBase.documentId + '_annotations_textMarkup', annotationStringified);
    //     } else {
    //         let annotObject: IPageAnnotations[] = JSON.parse(storeObject);
    //         window.sessionStorage.removeItem(this.pdfViewerBase.documentId + '_annotations_textMarkup');
    //         let pageIndex: number = this.pdfViewer.annotationModule.getPageCollection(annotObject, pageNumber);
    //         if (annotObject[pageIndex]) {
    //             (annotObject[pageIndex] as IPageAnnotations).annotations.push(annotation);
    //             index = (annotObject[pageIndex] as IPageAnnotations).annotations.indexOf(annotation);
    //         } else {
    //             let markupAnnotation: IPageAnnotations = { pageIndex: pageNumber, annotations: [] };
    //             markupAnnotation.annotations.push(annotation);
    //             index = markupAnnotation.annotations.indexOf(annotation);
    //             annotObject.push(markupAnnotation);
    //         }
    //         let annotationStringified: string = JSON.stringify(annotObject);
    //         window.sessionStorage.setItem(this.pdfViewerBase.documentId + '_annotations_textMarkup', annotationStringified);
    //     }
    //     return index;
    // }

    public saveAnnotations(pageAnnotations:ITextMarkupAnnotation[],pageNumber:number):void{
         const annotation : IPageAnnotations[] =[{pageIndex:pageNumber,annotations:pageAnnotations}];
        window.sessionStorage.setItem(this.pdfViewerBase.documentId + '_annotations_textMarkup', JSON.stringify( annotation));
    }
    public manageAnnotations(pageAnnotations: ITextMarkupAnnotation[], pageNumber: number): void {
        // tslint:disable-next-line
        let storeObject: any = window.sessionStorage.getItem(this.pdfViewerBase.documentId + '_annotations_textMarkup');
        if (storeObject) {
            const annotObject: IPageAnnotations[] = JSON.parse(storeObject);
            window.sessionStorage.removeItem(this.pdfViewerBase.documentId + '_annotations_textMarkup');
            const index: number = this.pdfViewer.annotationModule.getPageCollection(annotObject, pageNumber);
            if (annotObject[index]) {
                annotObject[index].annotations = pageAnnotations;
            }
            const annotationStringified: string = JSON.stringify(annotObject);
            window.sessionStorage.setItem(this.pdfViewerBase.documentId + '_annotations_textMarkup', annotationStringified);
        }
    }

    // tslint:disable-next-line
    private getAnnotations(pageIndex: number, textMarkupAnnotations: any[], id?: string): any[] {
        // tslint:disable-next-line
        let annotationCollection: any[];
        // tslint:disable-next-line
        if (id == null || id == undefined) {
            id = '_annotations_textMarkup';
        }
        // tslint:disable-next-line
        let storeObject: any = window.sessionStorage.getItem(this.pdfViewerBase.documentId + id);
        if (storeObject) {
            const annotObject: IPageAnnotations[] = JSON.parse(storeObject);
            const index: number = this.pdfViewer.annotationModule.getPageCollection(annotObject, pageIndex);
            if (annotObject[index]) {
                annotationCollection = annotObject[index].annotations;
            } else {
                annotationCollection = textMarkupAnnotations;
            }
        } else {
            annotationCollection = textMarkupAnnotations;
        }
        return annotationCollection;
    }

    // tslint:disable-next-line
    private getAddedAnnotation(type: string, color: string, opacity: number, bounds: any, author: string, subject: string, predefinedDate: string, note: string, rect: any, pageNumber: number): ITextMarkupAnnotation {
        const date: Date = new Date();
        // tslint:disable-next-line:max-line-length
        const modifiedDate: string = predefinedDate ? predefinedDate : date.toLocaleString();
        const annotationName: string = this.pdfViewer.annotation.createGUID();
        const commentsDivid: string = this.pdfViewer.annotation.stickyNotesAnnotationModule.addComments('textMarkup', pageNumber + 1, type);
        if (commentsDivid) {
            document.getElementById(commentsDivid).id = annotationName;
        }
        const annotation: ITextMarkupAnnotation = {
            // tslint:disable-next-line:max-line-length
            textMarkupAnnotationType: type, color, opacity, bounds, author, subject, modifiedDate, note, rect,
            annotName: annotationName, comments: [], review: { state: '', stateModel: '', author, modifiedDate }, shapeAnnotationType: 'textMarkup'
        };
        if (document.getElementById(annotationName)) {
            document.getElementById(annotationName).addEventListener('mouseup', this.annotationDivSelect(annotation, pageNumber));
        }
        const storedIndex: number = this.pdfViewer.annotationModule.storeAnnotations(pageNumber, annotation, '_annotations_textMarkup');
        this.pdfViewer.annotationModule.addAction(pageNumber, storedIndex, annotation, 'Text Markup Added', null);
        return annotation;
    }

    // tslint:disable-next-line
    private annotationDivSelect(annotation: any, pageNumber: number): any {
        const canvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageNumber);
        this.selectAnnotation(annotation, canvas, pageNumber);
        if (this.pdfViewer.toolbarModule) {
            if (this.pdfViewer.toolbarModule.annotationToolbarModule) {
                this.pdfViewer.toolbarModule.annotationToolbarModule.clearShapeMode();
                this.pdfViewer.toolbarModule.annotationToolbarModule.clearMeasureMode();
                this.pdfViewer.toolbarModule.annotationToolbarModule.enableTextMarkupAnnotationPropertiesTools(true);
                this.pdfViewer.toolbarModule.annotationToolbarModule.selectAnnotationDeleteItem(true);
                this.pdfViewer.toolbarModule.annotationToolbarModule.setCurrentColorInPicker();
                this.pdfViewer.toolbarModule.annotationToolbarModule.isToolbarHidden = true;
                this.pdfViewer.toolbarModule.annotationToolbarModule.showAnnotationToolbar(this.pdfViewer.toolbarModule.annotationItem);
            }
        }
    }

    private getPageContext(pageNumber: number): CanvasRenderingContext2D {
        const canvas: HTMLElement = this.pdfViewerBase.getElement('_annotationCanvas_' + pageNumber);
        let context: CanvasRenderingContext2D = null;
        if (canvas) {
            context = (canvas as HTMLCanvasElement).getContext('2d');
        }
        return context;
    }

    private getDefaultValue(value: number): number {
        return value / this.pdfViewerBase.getZoomFactor();
    }

    private getMagnifiedValue(value: number, factor: number): number {
        return value * factor;
    }

    /**
     * @private
     */
    // tslint:disable-next-line
    public saveImportedTextMarkupAnnotations(annotation: any, pageNumber: number): any {
        let annotationObject: ITextMarkupAnnotation = null;
        annotation.Author = this.pdfViewer.annotationModule.updateAnnotationAuthor('textMarkup', annotation.Subject);
        // tslint:disable-next-line:max-line-length
        annotationObject = {
            // tslint:disable-next-line:max-line-length
            textMarkupAnnotationType: annotation.TextMarkupAnnotationType, color: annotation.Color, opacity: annotation.Opacity, bounds: annotation.Bounds, author: annotation.Author, subject: annotation.Subject, modifiedDate: annotation.ModifiedDate, note: annotation.Note, rect: annotation.Rect,
            annotName: annotation.AnnotName, comments: this.pdfViewer.annotationModule.getAnnotationComments(annotation.Comments, annotation, annotation.Author), review: { state: annotation.State, stateModel: annotation.StateModel, modifiedDate: annotation.ModifiedDate, author: annotation.Author }, shapeAnnotationType: 'textMarkup'
        };
        this.pdfViewer.annotationModule.storeAnnotations(pageNumber, annotationObject, '_annotations_textMarkup');
    }

    /**
     * @private
     */
    public clear(): void {
        this.selectTextMarkupCurrentPage = null;
        this.currentTextMarkupAnnotation = null;
        window.sessionStorage.removeItem(this.pdfViewerBase.documentId + '_annotations_textMarkup');
    }
}