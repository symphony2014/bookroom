import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.less'],
})
export class IframeComponent implements OnInit {
  @Input()
  url: string;
  urlSafe: any;
  constructor(private sanitizer: DomSanitizer) {
    // this.urlSafe = sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

  ngOnInit() {
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }
}
