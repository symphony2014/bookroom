import {
  Component,
  ElementRef,
  ViewContainerRef,
  ChangeDetectionStrategy,
  Renderer2,
  Injector,
  ValueProvider,
} from '@angular/core';
import {
  ComponentBase,
  IComponentBase,
  applyMixins,
  ComponentMixins,
  PropertyCollectionInfo,
  setValue,
} from '@syncfusion/ej2-angular-base';
import { PdfViewer } from './pdfviewer/pdfviewer';
import { TextSelection } from './pdfviewer/text-selection';
import { Navigation } from './pdfviewer/navigation';
import { Annotation, LinkAnnotation } from './pdfviewer/annotation';
import { Magnification } from './pdfviewer/magnification';

export const inputs: string[] = [
  'annotations',
  'drawingObject',
  'tool',
  'ajaxRequestSettings',
  'annotationSelectorSettings',
  'annotationToolbarSettings',
  'areaSettings',
  'arrowSettings',
  'circleSettings',
  'contextMenuOption',
  'customStampItems',
  'customStampSettings',
  'distanceSettings',
  'documentPath',
  'enableAnnotation',
  'enableAnnotationToolbar',
  'enableBookmark',
  'enableCommentPanel',
  'enableDownload',
  'enableFormFields',
  'enableFreeText',
  'enableHandwrittenSignature',
  'enableHyperlink',
  'enableMagnification',
  'enableMeasureAnnotation',
  'enableNavigation',
  'enableNavigationToolbar',
  'enablePersistence',
  'enablePinchZoom',
  'enablePrint',
  'enableRtl',
  'enableShapeAnnotation',
  'enableShapeLabel',
  'enableStampAnnotations',
  'enableStickyNotesAnnotation',
  'enableTextMarkupAnnotation',
  'enableTextMarkupResizer',
  'enableTextSearch',
  'enableTextSelection',
  'enableThumbnail',
  'enableToolbar',
  'freeTextSettings',
  'handWrittenSignatureSettings',
  'height',
  'highlightSettings',
  'hyperlinkOpenState',
  'interactionMode',
  'lineSettings',
  'locale',
  'measurementSettings',
  'perimeterSettings',
  'polygonSettings',
  'radiusSettings',
  'rectangleSettings',
  'selectedItems',
  'serverActionSettings',
  'serviceUrl',
  'shapeLabelSettings',
  'stampSettings',
  'stickyNotesSettings',
  'strikethroughSettings',
  'textSearchColorSettings',
  'toolbarSettings',
  'underlineSettings',
  'volumeSettings',
  'width',
];
export const outputs: string[] = [
  'ajaxRequestFailed',
  'annotationAdd',
  'annotationPropertiesChange',
  'annotationRemove',
  'annotationResize',
  'annotationSelect',
  'documentLoad',
  'documentLoadFailed',
  'documentUnload',
  'exportFailed',
  'exportStart',
  'exportSuccess',
  'hyperlinkClick',
  'hyperlinkMouseOver',
  'importFailed',
  'importStart',
  'importSuccess',
  'pageChange',
  'pageClick',
  'textSelectionEnd',
  'textSelectionStart',
  'thumbnailClick',
  'zoomChange',
];
export const twoWays: string[] = [];

/**
 * `ejs-pdfviewer` represents the Angular PdfViewer Component.
 * ```html
 * <ejs-pdfviewer></ejs-pdfviewer>
 * ```
 */
@Component({
  selector: 'app-ejs-pdfviewer',
  inputs: inputs,
  outputs: outputs,
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
  queries: {},
})
@ComponentMixins([ComponentBase])
export class PdfViewerComponent extends PdfViewer implements IComponentBase {
  constructor(
    private ngEle: ElementRef,
    private srenderer: Renderer2,
    private viewContainerRef: ViewContainerRef,
    private injector: Injector,
  ) {
    super();
    this.element = this.ngEle.nativeElement;
    this.injectedModules = this.injectedModules || [];
    // try {
    //   const mod = this.injector.get('PdfViewerLinkAnnotation');
    //   if (this.injectedModules.indexOf(mod) === -1) {
    //     this.injectedModules.push(mod);
    //   }
    // } catch {}
     this.injectedModules.push(LinkAnnotation)
    try {
      const mod = this.injector.get('PdfViewerBookmarkView');
      if (this.injectedModules.indexOf(mod) === -1) {
        this.injectedModules.push(mod);
      }
    } catch {}

    // try {
    //   const mod = this.injector.get('PdfViewerMagnification');
    //   if (this.injectedModules.indexOf(mod) === -1) {
    //     this.injectedModules.push(mod);
    //   }
    // } catch {}
    this.injectedModules.push(Magnification);
    try {
      const mod = this.injector.get('PdfViewerThumbnailView');
      if (this.injectedModules.indexOf(mod) === -1) {
        this.injectedModules.push(mod);
      }
    } catch {}

    try {
      const mod = this.injector.get('PdfViewerToolbar');
      if (this.injectedModules.indexOf(mod) === -1) {
        this.injectedModules.push(mod);
      }
    } catch {}

    try {
      // const mod = this.injector.get('PdfViewerNavigation');
      // if (this.injectedModules.indexOf(mod) === -1) {
      //   this.injectedModules.push(mod);
      // }
      this.injectedModules.push(Navigation);
    } catch {}

    try {
      const mod = this.injector.get('PdfViewerPrint');
      if (this.injectedModules.indexOf(mod) === -1) {
        this.injectedModules.push(mod);
      }
    } catch {}

    try {
      // const mod = this.injector.get('PdfViewerTextSelection');
      // if (this.injectedModules.indexOf(mod) === -1) {
      //   this.injectedModules.push(mod);
      // }
      this.injectedModules.push(TextSelection);
    } catch {}

    try {
      const mod = this.injector.get('PdfViewerTextSearch');
      if (this.injectedModules.indexOf(mod) === -1) {
        this.injectedModules.push(mod);
      }
    } catch {}

    // try {
    //   const mod = this.injector.get('PdfViewerAnnotation');
    //   if (this.injectedModules.indexOf(mod) === -1) {
    //     this.injectedModules.push(mod);
    //   }
    // } catch {}

    this.injectedModules.push(Annotation);
    try {
      const mod = this.injector.get('PdfViewerFormFields');
      if (this.injectedModules.indexOf(mod) === -1) {
        this.injectedModules.push(mod);
      }
    } catch {}

    this.registerEvents(outputs);
    this.addTwoWay.call(this, twoWays);
    setValue('currentInstance', this, this.viewContainerRef);
  }

  public registerEvents: (eventList: string[]) => void;
  public addTwoWay: (propList: string[]) => void;

  public ngOnInit() {}

  public ngAfterViewInit(): void {}

  public ngOnDestroy(): void {}

  public ngAfterContentChecked(): void {}
}
