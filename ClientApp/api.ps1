

#lang: "typescript-angular"
#spec: {openapi: "3.0.1", info: {title: "My API", version: "v1"},…}
#type: "CLIENT"
$result = @"
{
  "lang": "typescript-angular",
  "type":"CLIENT"
}
"@

$postFix=Get-Date -Format "yyyyMMddHHmmss"
$name = $postFix+".zip"
$name
$resultObj = ConvertFrom-Json -InputObject $result
$json=Invoke-webRequest -Uri "http://localhost:5001/swagger/v1/swagger.json"
$resultObj | add-member -Name "spec" -value (Convertfrom-Json $json) -MemberType NoteProperty
$resultStr = ConvertTo-Json -depth 100 $resultObj
Invoke-WebRequest -outfile $name -Uri "https://generator3.swagger.io/api/generate" -Method "POST" -Headers @{"Accept"="application/json"; "Referer"="https://editor.swagger.io/?_ga=2.74160914.1099152069.1570751887-1109166536.1569714747"; "Origin"="https://editor.swagger.io"; "User-Agent"="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"; "Sec-Fetch-Mode"="cors"} -ContentType "application/json" -Body $resultStr

Expand-Archive $name -DestinationPath src/api -Force

(Get-Content src\api\model\operation.ts).replace('import { ModelObject } from ''./modelObject'';', '') | Set-Content src\api\model\operation.ts
(Get-Content src\api\model\operation.ts).replace('ModelObject', 'any') | Set-Content src\api\model\operation.ts
(Get-Content src\api\model\progress.ts).replace('import { OneOfProgressUser } from ''./oneOfProgressUser'';', '') | Set-Content src\api\model\progress.ts
(Get-Content src\api\model\progress.ts).replace('OneOfProgressUser', 'any') | Set-Content src\api\model\progress.ts