-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `library`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `library` /*!40100 DEFAULT CHARACTER SET utf8mb4   */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `library`;

--
-- Table structure for table `__efmigrationshistory`
--

DROP TABLE IF EXISTS `__efmigrationshistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `__efmigrationshistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__efmigrationshistory`
--

LOCK TABLES `__efmigrationshistory` WRITE;
/*!40000 ALTER TABLE `__efmigrationshistory` DISABLE KEYS */;
INSERT INTO `__efmigrationshistory` VALUES ('20191022135413_fix2','2.2.6-servicing.0'),('20191022230923_addprogress','2.2.6-servicing.0'),('20191024104125_progress','2.2.6-servicing.0'),('20191024111312_updateid','2.2.6-servicing.0'),('20191024120103_id','2.2.6-servicing.0'),('20191024231559_lib','2.2.6-servicing.0'),('20191025144753_numpages','2.2.6-servicing.0'),('20191026024253_latset','2.2.6-servicing.0');
/*!40000 ALTER TABLE `__efmigrationshistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aspnetroleclaims`
--

DROP TABLE IF EXISTS `aspnetroleclaims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aspnetroleclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` varchar(255) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  PRIMARY KEY (`Id`),
  KEY `IX_AspNetRoleClaims_RoleId` (`RoleId`),
  CONSTRAINT `FK_AspNetRoleClaims_AspNetRoles_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `aspnetroles` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspnetroleclaims`
--

LOCK TABLES `aspnetroleclaims` WRITE;
/*!40000 ALTER TABLE `aspnetroleclaims` DISABLE KEYS */;
INSERT INTO `aspnetroleclaims` VALUES (1,'df913b87-96c4-4c39-a1ea-4f1eda8bf65d','permission','users.view'),(2,'df913b87-96c4-4c39-a1ea-4f1eda8bf65d','permission','users.manage'),(3,'df913b87-96c4-4c39-a1ea-4f1eda8bf65d','permission','roles.view'),(4,'df913b87-96c4-4c39-a1ea-4f1eda8bf65d','permission','roles.manage'),(5,'df913b87-96c4-4c39-a1ea-4f1eda8bf65d','permission','roles.assign');
/*!40000 ALTER TABLE `aspnetroleclaims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aspnetroles`
--

DROP TABLE IF EXISTS `aspnetroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aspnetroles` (
  `Id` varchar(255) NOT NULL,
  `Name` varchar(256) DEFAULT NULL,
  `NormalizedName` varchar(256) DEFAULT NULL,
  `ConcurrencyStamp` longtext,
  `Description` longtext,
  `CreatedBy` longtext,
  `UpdatedBy` longtext,
  `CreatedDate` datetime(6) NOT NULL,
  `UpdatedDate` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `RoleNameIndex` (`NormalizedName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspnetroles`
--

LOCK TABLES `aspnetroles` WRITE;
/*!40000 ALTER TABLE `aspnetroles` DISABLE KEYS */;
INSERT INTO `aspnetroles` VALUES ('82630291-f65d-409a-936c-78c3fcf8058f','user','USER','f3442156-fa65-4002-8470-3b73694bb932','Default user',NULL,NULL,'2019-10-24 12:03:06.993560','2019-10-24 12:03:06.993560'),('df913b87-96c4-4c39-a1ea-4f1eda8bf65d','administrator','ADMINISTRATOR','d5fe1158-dd77-4615-86d4-6050f5ca11ed','Default administrator',NULL,NULL,'2019-10-24 12:03:06.731757','2019-10-24 12:03:06.968937');
/*!40000 ALTER TABLE `aspnetroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aspnetuserclaims`
--

DROP TABLE IF EXISTS `aspnetuserclaims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aspnetuserclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` varchar(255) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  PRIMARY KEY (`Id`),
  KEY `IX_AspNetUserClaims_UserId` (`UserId`),
  CONSTRAINT `FK_AspNetUserClaims_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspnetuserclaims`
--

LOCK TABLES `aspnetuserclaims` WRITE;
/*!40000 ALTER TABLE `aspnetuserclaims` DISABLE KEYS */;
/*!40000 ALTER TABLE `aspnetuserclaims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aspnetuserlogins`
--

DROP TABLE IF EXISTS `aspnetuserlogins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aspnetuserlogins` (
  `LoginProvider` varchar(255) NOT NULL,
  `ProviderKey` varchar(255) NOT NULL,
  `ProviderDisplayName` longtext,
  `UserId` varchar(255) NOT NULL,
  PRIMARY KEY (`LoginProvider`,`ProviderKey`),
  KEY `IX_AspNetUserLogins_UserId` (`UserId`),
  CONSTRAINT `FK_AspNetUserLogins_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspnetuserlogins`
--

LOCK TABLES `aspnetuserlogins` WRITE;
/*!40000 ALTER TABLE `aspnetuserlogins` DISABLE KEYS */;
/*!40000 ALTER TABLE `aspnetuserlogins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aspnetuserroles`
--

DROP TABLE IF EXISTS `aspnetuserroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aspnetuserroles` (
  `UserId` varchar(255) NOT NULL,
  `RoleId` varchar(255) NOT NULL,
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `IX_AspNetUserRoles_RoleId` (`RoleId`),
  CONSTRAINT `FK_AspNetUserRoles_AspNetRoles_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `aspnetroles` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_AspNetUserRoles_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspnetuserroles`
--

LOCK TABLES `aspnetuserroles` WRITE;
/*!40000 ALTER TABLE `aspnetuserroles` DISABLE KEYS */;
INSERT INTO `aspnetuserroles` VALUES ('f7d886d6-038c-4326-a3cf-e5cbee787fc9','82630291-f65d-409a-936c-78c3fcf8058f'),('c1cc4bba-e3cc-442d-8b21-8335c5cb94b3','df913b87-96c4-4c39-a1ea-4f1eda8bf65d');
/*!40000 ALTER TABLE `aspnetuserroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aspnetusers`
--

DROP TABLE IF EXISTS `aspnetusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aspnetusers` (
  `Id` varchar(255) NOT NULL,
  `UserName` varchar(256) DEFAULT NULL,
  `NormalizedUserName` varchar(256) DEFAULT NULL,
  `Email` varchar(256) DEFAULT NULL,
  `NormalizedEmail` varchar(256) DEFAULT NULL,
  `EmailConfirmed` bit(1) NOT NULL,
  `PasswordHash` longtext,
  `SecurityStamp` longtext,
  `ConcurrencyStamp` longtext,
  `PhoneNumber` longtext,
  `PhoneNumberConfirmed` bit(1) NOT NULL,
  `TwoFactorEnabled` bit(1) NOT NULL,
  `LockoutEnd` datetime(6) DEFAULT NULL,
  `LockoutEnabled` bit(1) NOT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `JobTitle` longtext,
  `FullName` longtext,
  `Configuration` longtext,
  `IsEnabled` bit(1) NOT NULL,
  `CreatedBy` longtext,
  `UpdatedBy` longtext,
  `CreatedDate` datetime(6) NOT NULL,
  `UpdatedDate` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UserNameIndex` (`NormalizedUserName`),
  KEY `EmailIndex` (`NormalizedEmail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspnetusers`
--

LOCK TABLES `aspnetusers` WRITE;
/*!40000 ALTER TABLE `aspnetusers` DISABLE KEYS */;
INSERT INTO `aspnetusers` VALUES ('c1cc4bba-e3cc-442d-8b21-8335c5cb94b3','admin','ADMIN','symphony2013@outlook.com','SYMPHONY2013@OUTLOOK.COM',_binary '','AQAAAAEAACcQAAAAEO8cFThYz8tBTjYDgDXn116Gb2lHkVNSxWeZ7D4lk0PTf+7b0dzfBz+COBdyjTfNnA==','L2TQLYRVMA45GBE55WRIJXVUWH5HEXHT','be8bc216-ac19-4fb9-a7e0-7e27237fb69e','15513092360',_binary '\0',_binary '\0',NULL,_binary '',0,NULL,'Inbuilt Administrator',NULL,_binary '',NULL,NULL,'2019-10-24 12:03:07.097362','2019-10-29 06:12:31.175055'),('f7d886d6-038c-4326-a3cf-e5cbee787fc9','user','USER','user@ebenmonney.com','USER@EBENMONNEY.COM',_binary '','AQAAAAEAACcQAAAAEFCqUj9OxsEoI4ehvUu40obItCae60VJ7lmhVMtt9/EV/JEBWNJV6H9HX4O4GjukxQ==','LVZ5J32DDJ2EQOLCL4XE6MATDTLLM2LC','0d932705-6a80-4532-8232-786b26912022','+1 (123) 000-0001',_binary '\0',_binary '\0',NULL,_binary '',0,NULL,'Inbuilt Standard User',NULL,_binary '',NULL,NULL,'2019-10-24 12:03:07.210645','2019-10-24 12:03:07.245228');
/*!40000 ALTER TABLE `aspnetusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aspnetusertokens`
--

DROP TABLE IF EXISTS `aspnetusertokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aspnetusertokens` (
  `UserId` varchar(255) NOT NULL,
  `LoginProvider` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Value` longtext,
  PRIMARY KEY (`UserId`,`LoginProvider`,`Name`),
  CONSTRAINT `FK_AspNetUserTokens_AspNetUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspnetusertokens`
--

LOCK TABLES `aspnetusertokens` WRITE;
/*!40000 ALTER TABLE `aspnetusertokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `aspnetusertokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `books` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` longtext,
  `Path` longtext,
  `UserId` longtext,
  `UploadUserId` varchar(255) DEFAULT NULL,
  `NumberOfPages` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `IX_Books_UploadUserId` (`UploadUserId`),
  CONSTRAINT `FK_Books_AspNetUsers_UploadUserId` FOREIGN KEY (`UploadUserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (3,'math-deep.pdf','books\\computerScience\\math-deep.pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,1962),(4,'math-deep.pdf','books\\computerScience\\math-deep.pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,1962),(5,'math-deep.pdf','books\\computerScience\\math-deep.pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,1962),(6,'hartmann2014.pdf','books\\computerScience\\hartmann2014.pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,12),(7,'mcs.pdf','books\\computerScience\\mcs.pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,1006),(8,'Cracking the Coding Interview.pdf','books\\computerScience\\Cracking the Coding Interview.pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,310),(9,'Gayle Laakmann McDowell - Cracking the Coding Interview_ 189 Programming Questions and Solutions-CareerCup (2015).pdf','books\\computerScience\\Gayle Laakmann McDowell - Cracking the Coding Interview_ 189 Programming Questions and Solutions-CareerCup (2015).pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,708),(10,'(Addison-Wesley Object Technology Series) Martin Fowler - Refactoring_ Improving the Design of Existing Code-Addison-Wesley Professional (2018).pdf','books\\computerScience\\(Addison-Wesley Object Technology Series) Martin Fowler - Refactoring_ Improving the Design of Existing Code-Addison-Wesley Professional (2018).pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,455),(11,'(Lecture Notes in Computer Science 3923 _ Theoretical Computer Science and General Issues) George Necula (auth.), Alan Mycroft, Andreas Zeller (eds.) - Compiler Construction_ 15th International Confer.pdf','books\\computerScience\\(Lecture Notes in Computer Science 3923 _ Theoretical Computer Science and General Issues) George Necula (auth.), Alan Mycroft, Andreas Zeller (eds.) - Compiler Construction_ 15th International Confer.pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,288),(12,'A Generic Worklist Algorithm.pdf','books\\computerScience\\A Generic Worklist Algorithm.pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,143),(13,'Konrad Kokosa - Pro .NET Memory Management_ For Better Code, Performance, and Scalability-Apress (2018).pdf','books\\computerScience\\Konrad Kokosa - Pro .NET Memory Management_ For Better Code, Performance, and Scalability-Apress (2018).pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,1091),(14,'Dmitri Nesteruk - Design Patterns in .NET_ Reusable Approaches in C# and F# for Object-Oriented Software Design-Apress (2019).pdf','books\\computerScience\\Dmitri Nesteruk - Design Patterns in .NET_ Reusable Approaches in C# and F# for Object-Oriented Software Design-Apress (2019).pdf','c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',NULL,356);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `openiddictapplications`
--

DROP TABLE IF EXISTS `openiddictapplications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `openiddictapplications` (
  `Id` varchar(255) NOT NULL,
  `ClientId` varchar(100) NOT NULL,
  `ClientSecret` longtext,
  `ConcurrencyToken` varchar(50) DEFAULT NULL,
  `ConsentType` longtext,
  `DisplayName` longtext,
  `Permissions` longtext,
  `PostLogoutRedirectUris` longtext,
  `Properties` longtext,
  `RedirectUris` longtext,
  `Type` varchar(25) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IX_OpenIddictApplications_ClientId` (`ClientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `openiddictapplications`
--

LOCK TABLES `openiddictapplications` WRITE;
/*!40000 ALTER TABLE `openiddictapplications` DISABLE KEYS */;
INSERT INTO `openiddictapplications` VALUES ('090202ff-8f99-4657-9fc3-8c23b6e7bc50','web','AQAAAAEAACcQAAAAEE/SxSKyrFVRwI/n9ZhG/ZeB0aIwXLu+UOdwAB91xgfFAoMhKAdPB3DI3yErHirNTQ==','18f89985-068c-42b9-af7c-2bb4737fd7db',NULL,'My client application','[\"ept:token\",\"gt:client_credentials\",\"gt:password\",\"offline_access\",\"openid\",\"email\",\"phone\",\"profile\",\"roles\"]',NULL,NULL,NULL,'confidential');
/*!40000 ALTER TABLE `openiddictapplications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `openiddictauthorizations`
--

DROP TABLE IF EXISTS `openiddictauthorizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `openiddictauthorizations` (
  `Id` varchar(255) NOT NULL,
  `ApplicationId` varchar(255) DEFAULT NULL,
  `ConcurrencyToken` varchar(50) DEFAULT NULL,
  `Properties` longtext,
  `Scopes` longtext,
  `Status` varchar(25) NOT NULL,
  `Subject` varchar(450) NOT NULL,
  `Type` varchar(25) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_OpenIddictAuthorizations_ApplicationId_Status_Subject_Type` (`ApplicationId`,`Status`,`Subject`,`Type`),
  CONSTRAINT `FK_OpenIddictAuthorizations_OpenIddictApplications_ApplicationId` FOREIGN KEY (`ApplicationId`) REFERENCES `openiddictapplications` (`Id`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `openiddictauthorizations`
--

LOCK TABLES `openiddictauthorizations` WRITE;
/*!40000 ALTER TABLE `openiddictauthorizations` DISABLE KEYS */;
/*!40000 ALTER TABLE `openiddictauthorizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `openiddictscopes`
--

DROP TABLE IF EXISTS `openiddictscopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `openiddictscopes` (
  `Id` varchar(255) NOT NULL,
  `ConcurrencyToken` varchar(50) DEFAULT NULL,
  `Description` longtext,
  `DisplayName` longtext,
  `Name` varchar(200) NOT NULL,
  `Properties` longtext,
  `Resources` longtext,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IX_OpenIddictScopes_Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `openiddictscopes`
--

LOCK TABLES `openiddictscopes` WRITE;
/*!40000 ALTER TABLE `openiddictscopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `openiddictscopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `openiddicttokens`
--

DROP TABLE IF EXISTS `openiddicttokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `openiddicttokens` (
  `Id` varchar(255) NOT NULL,
  `ApplicationId` varchar(255) DEFAULT NULL,
  `AuthorizationId` varchar(255) DEFAULT NULL,
  `ConcurrencyToken` varchar(50) DEFAULT NULL,
  `CreationDate` datetime(6) DEFAULT NULL,
  `ExpirationDate` datetime(6) DEFAULT NULL,
  `Payload` longtext,
  `Properties` longtext,
  `ReferenceId` varchar(100) DEFAULT NULL,
  `Status` varchar(25) NOT NULL,
  `Subject` varchar(450) NOT NULL,
  `Type` varchar(25) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IX_OpenIddictTokens_ReferenceId` (`ReferenceId`),
  KEY `IX_OpenIddictTokens_AuthorizationId` (`AuthorizationId`),
  KEY `IX_OpenIddictTokens_ApplicationId_Status_Subject_Type` (`ApplicationId`,`Status`,`Subject`,`Type`),
  CONSTRAINT `FK_OpenIddictTokens_OpenIddictApplications_ApplicationId` FOREIGN KEY (`ApplicationId`) REFERENCES `openiddictapplications` (`Id`) ON DELETE RESTRICT,
  CONSTRAINT `FK_OpenIddictTokens_OpenIddictAuthorizations_AuthorizationId` FOREIGN KEY (`AuthorizationId`) REFERENCES `openiddictauthorizations` (`Id`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `openiddicttokens`
--

LOCK TABLES `openiddicttokens` WRITE;
/*!40000 ALTER TABLE `openiddicttokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `openiddicttokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `progress`
--

DROP TABLE IF EXISTS `progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `progress` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(255) DEFAULT NULL,
  `BookID` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_Progress_UserID` (`UserID`),
  KEY `IX_Progress_BookID` (`BookID`),
  CONSTRAINT `FK_Progress_AspNetUsers_UserID` FOREIGN KEY (`UserID`) REFERENCES `aspnetusers` (`Id`) ON DELETE RESTRICT,
  CONSTRAINT `FK_Progress_Books_BookID` FOREIGN KEY (`BookID`) REFERENCES `books` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `progress`
--

LOCK TABLES `progress` WRITE;
/*!40000 ALTER TABLE `progress` DISABLE KEYS */;
INSERT INTO `progress` VALUES (3,NULL,4,0),(4,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',5,19),(5,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',6,1),(6,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',7,4),(7,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',8,5),(8,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',9,101),(9,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',10,0),(10,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',11,0),(11,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',12,0),(12,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',13,494),(13,'c1cc4bba-e3cc-442d-8b21-8335c5cb94b3',14,161);
/*!40000 ALTER TABLE `progress` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-22 19:17:07
